"""
In this file, there is all the code needed to run our server,
process each request coming from a client.
"""

import binascii
import hashlib
import json
import os
from os import path

from PIL import Image, ImageDraw, ImageFont
from flask import Flask, render_template, jsonify
from flask import redirect, url_for
from flask import request
from flask import session
from flask_sqlalchemy import SQLAlchemy

from sparql_query.queries import *

app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.jinja_env.auto_reload = True
db = SQLAlchemy(app)

from models import *

markerIDPattern = {
    38: "static/marker_pattern/pattern-aruco-38.patt",
    13: "static/marker_pattern/pattern-aruco-13.patt",
    73: "static/marker_pattern/pattern-aruco-73.patt",
    10: "static/marker_pattern/pattern-aruco-10.patt",
    76: "static/marker_pattern/pattern-aruco-76.patt",
    6: "static/marker_pattern/pattern-aruco-6.patt",
    80: "static/marker_pattern/pattern-aruco-80.patt",
    65: "static/marker_pattern/pattern-aruco-65.patt",
    17: "static/marker_pattern/pattern-aruco-17.patt",
    2: "static/marker_pattern/pattern-aruco-2.patt",
    77: "static/marker_pattern/pattern-aruco-77.patt",
    72: "static/marker_pattern/pattern-aruco-72.patt"
}


@app.route("/", methods=['GET', 'POST'])
def index():
    return render_template("index.html")


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user = db.session.query(User).filter(User.username == username).first()
        if (user is None) or not validate_password(user.password, password):
            error = 'Invalid Credentials. Please try again.'
        else:
            session.clear()
            session["user_id"] = username.capitalize()
            session["username"] = username
            session['logged_in'] = True
            return redirect(url_for('index'))
    return render_template('login.html', error=error)


@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for('index'))


@app.route("/register", methods=['GET', 'POST'])
def register():
    error = None
    if request.method == 'POST':
        username = request.form["username"]
        password = request.form["password"]
        password_confirmation = request.form["password_confirmation"]
        if password == password_confirmation:
            password = encrypt_password(password)
            user = User(username=username, password=password)
            try:
                db.session.add(user)
                db.session.commit()
            except:
                return render_template('register.html', error='User {} is already registered.'.format(username))
            return redirect(url_for('login'))
        else:
            session.clear()
            error = "Password doesn't match with Password Confirmation"
    return render_template('register.html', error=error)


def encrypt_password(password):
    salt_key = hashlib.sha256(os.urandom(1000)).hexdigest().encode('ascii')
    encrypted_password = hashlib.pbkdf2_hmac('md5', password.encode('utf-8'), salt_key, 1000000)
    encrypted_password = binascii.hexlify(encrypted_password)
    return (salt_key + encrypted_password).decode('ascii')


def validate_password(encrypted_stored_password, provided_password):
    salt_key = encrypted_stored_password[:64]
    encrypted_stored_password = encrypted_stored_password[64:]
    provided_password = hashlib.pbkdf2_hmac('md5', provided_password.encode('utf-8'), salt_key.encode('ascii'), 1000000)
    provided_password = binascii.hexlify(provided_password).decode('ascii')
    return provided_password == encrypted_stored_password


@app.route("/artworkScanner", methods=["GET"])
def artworkScanner():
    return render_template("artworkScanner.html")


@app.route("/artist_info", methods=["POST"])
def artist_info():
    return jsonify(query_basic_info_artist(request.form["artwork_name"]))


@app.route("/advanced_artwork_info", methods=["POST"])
def advanced_artwork_info():
    return jsonify(query_advanced_info_artwork(request.form["artwork_name"]))


@app.route("/artwork_basic_other_info", methods=["POST"])
def artwork_basic_other_info():
    if request.method == "POST":
        if "year" in request.form:
            key = "list artwork same century"
            value = query_artwork_list_same_century(request.form["year"])
        else:
            key = "list artwork same author in same museum"
            value = query_artwork_same_author_same_museum(request.form["artwork_name"], request.form["artwork_museum"],
                                                          request.form["artist"])
        return jsonify({key: value})


@app.route("/artwork_info", methods=["POST"])
def artwork_info():
    if "marker" in request.form:
        artwork = db.session.query(ArtworkMarkerID).filter(
            ArtworkMarkerID.identifier == int(request.form["marker"])).first()
        if artwork:
            if artwork.artwork_presence == 1:
                session["artwork_presence"] = "false"
            else:
                session["artwork_presence"] = "true"
            session["markerID"] = int(request.form["marker"])
            session["scan_marker"] = "true"
            session["markerPath"] = markerIDPattern[int(request.form["marker"])]
            session["artwork_art_movement"] = artwork.artwork_art_movement
            res_query = query(artwork.artwork_name)
            res_query[0]["artwork_art_movement"] = session["artwork_art_movement"]
            request_search = ArtworkRequestHistory(username=session["username"],
                                                   artwork_name=artwork.artwork_name)
            try:
                db.session.add(request_search)
                db.session.commit()
            except:
                pass
            basic_info_artwork = res_query[0]
            if "javascriptRequest" in request.form:
                return jsonify(basic_info_artwork)
            else:
                return render_template("ar_view.html",
                                       basic_info_artwork=basic_info_artwork)
    else:
        check_artwork_presence(request.form["artwork_name"])
        res_query = query(request.form["artwork_name"])
        request_search = ArtworkRequestHistory(username=session["username"],
                                               artwork_name=request.form["artwork_name"])
        try:
            db.session.add(request_search)
            db.session.commit()
        except:
            pass
        basic_info_artwork = res_query[0]
        return jsonify(basic_info_artwork)


def check_artwork_presence(artwork_name_to_check):
    artwork = db.session.query(ArtworkMarkerID).filter(
        ArtworkMarkerID.identifier == int(session["markerID"])).first()
    if artwork:
        if artwork.artwork_name == artwork_name_to_check:
            if artwork.artwork_presence == 1:
                session["artwork_presence"] = "false"
            else:
                session["artwork_presence"] = "true"
        else:
            session["artwork_presence"] = "false"
    else:
        session["artwork_presence"] = "false"


@app.route("/data_vis", methods=["POST"])
def data_vis():
    if request.method == "POST":
        if "artwork_name" in request.form:
            res_query = query(request.form["artwork_name"])
            request_search = ArtworkRequestHistory(username=session["username"],
                                                   artwork_name=request.form["artwork_name"])
            try:
                db.session.add(request_search)
                db.session.commit()
            except:
                pass
            return render_template("data_vis.html",
                                   basic_info_artwork=res_query[0]
                                   )
        else:
            session["scan_marker"] = "false"
            basic_info_artwork = json.loads(request.form["basic_info_artwork"])
            basic_info_artist = json.loads(request.form["basic_info_artist"])
            advanced_info_artwork = json.loads(request.form["advanced_info_artwork"])
            return render_template("data_vis.html",
                                   basic_info_artwork=basic_info_artwork,
                                   basic_info_artist=basic_info_artist,
                                   advanced_info_artwork=advanced_info_artwork)


@app.route('/info_artist', methods=["POST"])
def info_artist():
    info_artist = info_artist_from_name(request.form["data"])
    return jsonify(info_artist)


@app.route("/ar_view", methods=["GET", "POST"])
def ar_view():
    basic_info_artwork = None
    basic_info_artist = None
    advanced_info_artwork = None
    if request.method == "POST":
        session["scan_marker"] = "false"
        basic_info_artwork = json.loads(request.form["basic_info_artwork"])
        basic_info_artist = json.loads(request.form["basic_info_artist"])
        advanced_info_artwork = json.loads(request.form["advanced_info_artwork"])
        check_artwork_presence(basic_info_artwork["title"])
    return render_template("ar_view.html",
                           basic_info_artwork=basic_info_artwork,
                           basic_info_artist=basic_info_artist,
                           advanced_info_artwork=advanced_info_artwork)


@app.route("/custom_search", methods=["GET", "POST"])
def custom_search():
    if request.method == "POST":
        data = [request.form["artwork_list"], request.form["type"], request.form["keyword"]]
        return render_template("custom_search.html", data=data)
    else:
        return render_template("custom_search.html")


@app.route("/process_search", methods=["POST"])
def process_search():
    data = json.loads(request.form["data"])
    query = None
    if data[1] == "material":
        if data[0] == "yes":
            query = SEARCH_ARTWORK_MATERIAL_QUERY
        else:
            query = SEARCH_MATERIAL_QUERY
    elif data[1] == "type_technique":
        if data[0] == "yes":
            query = SEARCH_ARTWORK_TYPE_QUERY
        else:
            query = SEARCH_TYPE_QUERY
    elif data[1] == "art_mov":
        if data[0] == "no":
            query = SEARCH_ART_MOVEMENT_QUERY
    elif data[1] == "museum":
        if data[0] == "yes":
            query = SEARCH_ARTWORK_LIST_MUSEUM_QUERY
        else:
            query = SEARCH_MUSEUM_QUERY
    elif data[1] == "artist":
        if data[0] == "yes":
            query = SEARCH_ARTWORK_ARTIST
        else:
            query = SEARCH_ARTIST_QUERY
    elif data[1] == "artwork":
        query = SEARCH_ARTWORK_QUERY
    if query is not None:
        res_query = execute_query(query, search_data=data[2])
        request_search = CustomSearchHistory(username=session["username"],
                                             search=data[2],
                                             parameters="{};{}".format(data[1], data[0]))
        try:
            db.session.add(request_search)
            db.session.commit()
        except:
            pass
    else:
        res_query = ["No result"]
    return jsonify(res_query)


@app.route("/history", methods=["GET"])
def history():
    artwork_request_history = db.session.query(ArtworkRequestHistory).filter(
        ArtworkRequestHistory.username == session["username"]).all()
    artwork_request_history = [{"artwork": res.artwork_name} for res in artwork_request_history]
    custom_search_history = db.session.query(CustomSearchHistory).filter(
        CustomSearchHistory.username == session["username"]).all()
    custom_search_history = [{"search": res.search, "parameters": res.parameters.split(";")} for res in
                             custom_search_history]
    return render_template('history.html',
                           artwork_request_history=artwork_request_history,
                           custom_search_history=custom_search_history)


def query(artworkName):
    return [query_basic_info_artwork(artwork=artworkName)]


@app.route('/process_abstract', methods=["POST"])
def process_abstract():
    title = request.form["title"].replace(" ", "_")
    picture_path = "./static/img/{}_abstract_{}.png".format(request.form["type"], title)
    if not path.exists(picture_path):
        data = request.form["data"].split(' ')
        data_list = []
        line = ""
        for w in data:
            if len(line.replace(" ", "")) >= 120:
                data_list.append(line)
                line = ""
            line += w + " "
        if line:
            data_list.append(line)
        font = ImageFont.truetype('./static/font/arial.ttf', 24)
        size = font.getsize_multiline("\n".join(data_list))
        img = Image.new("RGB", (size[0] + 50, size[1] + 50), (255, 255, 255))
        d = ImageDraw.Draw(img)
        d.text((25, 25), "\n".join(data_list), font=font, fill=(0, 0, 0))
        img.save(picture_path)
    image = Image.open(picture_path)
    w, h = image.size
    return jsonify({"path": picture_path, "w": w, "h": h})


if __name__ == '__main__':
    app.run(debug=True)
