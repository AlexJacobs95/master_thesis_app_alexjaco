"""
This file contains the code needed to fill our database with selected artworks.
"""
import os

from app import app, db
from models import ArtworkMarkerID

app.config.from_object(os.environ['APP_SETTINGS'])

list_artwork_name = [
    ("Mona Lisa", 'High Renaissance'),
    ("Campbell's Soup Cans", "Pop Art"),
    ("Christ of Saint John of the Cross", "Surrealism"),
    ("Guernica (Picasso)", "Cubism"),
    ("The Weeping Woman", "Cubism"),
    ("The Starry Night", "Post-Impressionism"),
    ("The Night Watch", "Baroque"),
    ("The Ghost of Vermeer of Delft Which Can Be Used As a Table", "Surrealism"),
    ("Whistler's Mother", "Realism"),
    ("Pietà (Michelangelo)", "High Renaissance"),
    ("David (Michelangelo)", "High Renaissance"),
    ("The Thinker", "Impressionism"),
]
list_artwork_id = [38, 13, 73, 10, 76, 6, 80, 65, 17, 2, 77, 72]

if __name__ == '__main__':
    artwork_id_name = dict(zip(list_artwork_id, list_artwork_name))
    for key in artwork_id_name:
        artwork_marker_id = ArtworkMarkerID(key, artwork_id_name[key][0], artwork_id_name[key][1])
        try:
            db.session.add(artwork_marker_id)
            db.session.commit()
        except:
            pass
