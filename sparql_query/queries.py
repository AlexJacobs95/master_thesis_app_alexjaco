"""
In this file, you can fin all the python code to do the SPARQL queries
to the DBpedia endpoint.
"""

import googletrans
from SPARQLWrapper import SPARQLWrapper, JSON

from sparql_query.sparlq_queries import *


def execute_query(query, artwork=None, artist_name=None, museum_name=None, artMovement=None, year_range=None,
                  search_data=None, lang="en",
                  endpoint="http://dbpedia.org/sparql"):
    sparql = SPARQLWrapper(endpoint)
    if artwork is not None:
        if artwork.find('"') != -1:
            query = query.replace("<", "'")
            query = query.replace(">", "'")
        elif artwork.find("'") != -1:
            query = query.replace("<", '"')
            query = query.replace(">", '"')
        else:
            query = query.replace("<", '"')
            query = query.replace(">", '"')
        query = query.replace('artwork_name', artwork)
    query = query.replace("LANG", lang)
    if artist_name is not None:
        query = query.replace('artist_name', artist_name)
    if museum_name is not None:
        query = query.replace('museum_name', museum_name)
    if artMovement is not None:
        query = query.replace('artMovement', artMovement)
    if year_range is not None:
        query = query.replace('YEAR_START', str(year_range[0]))
        query = query.replace('YEAR_END', str(year_range[1]))
    if search_data is not None:
        query = query.replace('search_data', search_data)
    query = QUERY_HEADER + "\n\n" + query
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    return res["results"]["bindings"]


def query_artwork_french_name(artwork):
    artwork_french_name = execute_query(ARTWORK_FR_NAME_QUERY, artwork)
    if artwork_french_name:
        check_if_artwork_name_good = execute_query(CHECK_ARTWORK,
                                                   artwork=artwork_french_name[0]["french_name_artwork"]["value"])
        if not check_if_artwork_name_good:
            artwork_french_name = execute_query(ARTWORK_FR_NAME_QUERY, artwork, endpoint="http://fr.dbpedia.org/sparql")
    if not artwork_french_name:
        artwork_french_name = None
    else:
        artwork_french_name = artwork_french_name[0]["french_name_artwork"]["value"]
    return artwork_french_name


def query_author_name(artwork, artwork_french_name):
    author_name_dict = execute_query(AUTHOR_NAME_QUERY, artwork)
    if not author_name_dict and artwork_french_name is not None:
        author_name_dict = execute_query(AUTHOR_NAME_QUERY, artwork=artwork_french_name, lang="fr",
                                         endpoint="http://fr.dbpedia.org/sparql")
    if author_name_dict and len(author_name_dict[0]) == 2:
        author_name_dict = author_name_dict[0]['good_author_name']["value"]
    elif author_name_dict and len(author_name_dict[0]) == 1:
        author_name_dict = author_name_dict[0]['author_name']["value"]
    else:
        author_name_dict = None
    return author_name_dict


def query_artwork_abstract(artwork, artwork_french_name):
    abstract_comment_dict = execute_query(ABSTRACT_QUERY, artwork)
    if not abstract_comment_dict and artwork_french_name is not None:
        abstract_comment_dict = execute_query(ABSTRACT_QUERY, artwork=artwork_french_name, lang="fr",
                                              endpoint="http://fr.dbpedia.org/sparql")
    if not abstract_comment_dict:
        abstract_comment_dict = None
    else:
        abstract_comment_dict = abstract_comment_dict[0]["abstract"]["value"]
    return abstract_comment_dict


def query_artwork_year(artwork, artwork_french_name):
    year_query = execute_query(ARTWORK_YEAR_QUERY, artwork)
    if not year_query and artwork_french_name is not None:
        year_query = execute_query(ARTWORK_YEAR_QUERY_FR, artwork=artwork_french_name, lang="fr",
                                   endpoint="http://fr.dbpedia.org/sparql")
    if not year_query:
        year_query = None
    else:
        year_query = year_query[0]["year"]["value"]
    return year_query

def query_artwork_list_same_century(year):
    year_range = rangeYearCentury(year)
    if year_range is not None:
        list_artwork_same_century_en = execute_query(ARTWORK_LIST_SAME_CENTURY, year_range=year_range)
        list_artwork_same_century_fr = execute_query(ARTWORK_LIST_SAME_CENTURY, year_range=year_range,
                                                     endpoint="http://fr.dbpedia.org/sparql")
        list_artwork_same_century_en = [r["artworkName"]["value"] for r in list_artwork_same_century_en]
        list_artwork_same_century_fr = [r["artworkName"]["value"] for r in list_artwork_same_century_fr]
        list_artwork_same_century = list(set().union(list_artwork_same_century_en, list_artwork_same_century_fr))
        list_artwork_same_century = sorted(list_artwork_same_century)
    else:
        list_artwork_same_century = None
    return list_artwork_same_century

def query_artwork_museum(artwork, artwork_french_name):
    artwork_museum = execute_query(ARTWORK_MUSEUM_QUERY, artwork=artwork)
    if not artwork_museum and artwork_french_name is not None:
        artwork_museum = execute_query(ARTWORK_MUSEUM_QUERY, artwork=artwork_french_name, lang="fr",
                                       endpoint="http://fr.dbpedia.org/sparql")
    if not artwork_museum:
        artwork_museum = None
    else:
        artwork_museum = artwork_museum[0]["museum"]["value"]
    return artwork_museum

def query_artwork_same_author_same_museum(artwork, artwork_museum, author_name_dict):
    if artwork_museum is not None and author_name_dict is not None:
        artwork_museum_list_en = execute_query(ARTWORK_LIST_MUSEUM_QUERY, artist_name=author_name_dict,
                                               museum_name=artwork_museum)
        artwork_museum_list_fr = execute_query(ARTWORK_LIST_MUSEUM_QUERY, artist_name=author_name_dict,
                                               museum_name=artwork_museum, endpoint="http://fr.dbpedia.org/sparql")
        artwork_museum_list_en = [r["artworkName"]["value"] for r in artwork_museum_list_en]
        artwork_museum_list_fr = [r["artworkName"]["value"] for r in artwork_museum_list_fr]
        list_artwork_same_author_museum = list(set().union(artwork_museum_list_en, artwork_museum_list_fr))
        list_artwork_same_author_museum = sorted(list_artwork_same_author_museum)
        try:
            list_artwork_same_author_museum.remove(artwork)
        except Exception:
            pass
        if not list_artwork_same_author_museum:
            list_artwork_same_author_museum = None
    else:
        list_artwork_same_author_museum = None
    return list_artwork_same_author_museum

def query_type_mat_artwork(artwork, artwork_french_name):
    artwork_material = execute_query(ARTWORK_MATERIAL_QUERY, artwork=artwork)
    if not artwork_material and artwork_french_name is not None:
        artwork_material = execute_query(ARTWORK_MATERIAL_QUERY, artwork=artwork_french_name, lang="fr",
                                         endpoint="http://fr.dbpedia.org/sparql")
    if artwork_material:
        for idx, r in enumerate(artwork_material):
            detect = googletrans.Translator().detect(text=r["materialName"]["value"])
            if detect.lang != "en":
                artwork_material[idx]["materialName"]["value"] = googletrans.Translator().translate(
                    text=r["materialName"]["value"])
        artwork_material = [r["materialName"]["value"] for r in artwork_material]
    else:
        artwork_material = None

    artwork_painting_type = execute_query(ARTWORK_TYPE_QUERY, artwork=artwork)
    if not artwork_painting_type and artwork_french_name is not None:
        artwork_painting_type = execute_query(ARTWORK_TYPE_QUERY, artwork=artwork_french_name, lang="fr",
                                              endpoint="http://fr.dbpedia.org/sparql")
    if artwork_painting_type:
        for idx, r in enumerate(artwork_painting_type):
            detect = googletrans.Translator().detect(text=r["typeName"]["value"])
            if detect.lang != "en":
                artwork_painting_type[idx]["typeName"]["value"] = googletrans.Translator().translate(
                    text=r["typeName"]["value"]).text
        artwork_painting_type = [r["typeName"]["value"] for r in artwork_painting_type]
    else:
        artwork_painting_type = None

    if artwork_painting_type is not None and artwork_material is not None:
        artwork_painting_type_mat = artwork_material + artwork_painting_type
    elif artwork_painting_type is None and artwork_material is not None:
        artwork_painting_type_mat = artwork_material
    elif artwork_material is None and artwork_painting_type is not None:
        artwork_painting_type_mat = artwork_painting_type
    else:
        artwork_painting_type_mat = None

    return artwork_painting_type_mat


def query_artwork_movement(author_name_dict):
    author_art_movement = execute_query(AUTHOR_ARTMOVEMENT_QUERY, artist_name=author_name_dict)
    if not author_art_movement:
        author_art_movement = execute_query(AUTHOR_ARTMOVEMENT_QUERY, artist_name=author_name_dict,
                                            endpoint="http://fr.dbpedia.org/sparql")
    if not author_art_movement:
        author_art_movement = None
    else:
        author_art_movement = [r["art_movement"]["value"] for r in author_art_movement]
        if len(author_art_movement) > 1:
            author_art_movement = "More than one possible art movement for the author! Unable to determine the style of the artwork with precision"
        else:
            author_art_movement = ", ".join(author_art_movement)
    return author_art_movement


def query_picture_artwork(artwork, artwork_french_name):
    artwork_picture = execute_query(ARTWORK_PICTURE_QUERY, artwork=artwork)
    if not artwork_picture and artwork_french_name is not None:
        artwork_picture = execute_query(ARTWORK_PICTURE_QUERY, artwork=artwork_french_name, lang="fr",
                                        endpoint="http://fr.dbpedia.org/sparql")
    if not artwork_picture:
        artwork_picture = None
    else:
        artwork_picture = artwork_picture[0]["picture"]["value"]
    return artwork_picture


def query_basic_info_artwork(artwork):
    artwork_french_name = query_artwork_french_name(artwork)
    artwork_painting_type_mat = query_type_mat_artwork(artwork, artwork_french_name)
    author_name_dict = query_author_name(artwork, artwork_french_name)
    author_art_movement = query_artwork_movement(author_name_dict)
    abstract_comment_dict = query_artwork_abstract(artwork, artwork_french_name)
    artwork_picture = query_picture_artwork(artwork, artwork_french_name)
    artwork_museum = query_artwork_museum(artwork, artwork_french_name)
    year_query = query_artwork_year(artwork, artwork_french_name)
    json_dict = {"title": artwork,
                 "material type": artwork_painting_type_mat,
                 "artist": author_name_dict,
                 "art movement": author_art_movement,
                 "abstract": abstract_comment_dict,
                 "artwork picture": artwork_picture,
                 "year": year_query,
                 "museum": artwork_museum
                 # "list artwork same author in same museum": list_artwork_same_author_museum
                 # "list artwork same century": list_artwork_same_century
                 }
    return json_dict

def query_author_gender(author_name_dict):
    author_gender = execute_query(AUTHOR_GENDER_QUERY, artist_name=author_name_dict)
    if not author_gender:
        author_gender = execute_query(AUTHOR_GENDER_QUERY, artist_name=author_name_dict,
                                      endpoint="http://fr.dbpedia.org/sparql")
    if not author_gender:
        author_gender = None
    else:
        author_gender = author_gender[0]["gender"]["value"]
    return author_gender


def query_author_abstract(author_name_dict):
    author_abstract = execute_query(AUTHOR_ABSTRACT_QUERY, artist_name=author_name_dict)
    if not author_abstract:
        author_abstract = execute_query(AUTHOR_ABSTRACT_QUERY, artist_name=author_name_dict,
                                        endpoint="http://fr.dbpedia.org/sparql")
    if not author_abstract:
        author_abstract = None
    else:
        author_abstract = author_abstract[0]["abstract"]["value"]
    return author_abstract


def query_basic_info_artist(artwork):
    artwork_french_name = query_artwork_french_name(artwork)
    author_name_dict = query_author_name(artwork, artwork_french_name)
    json_dict = info_artist_from_name(author_name_dict)
    return json_dict

def query_art_mov_author(author_name_dict):
    author_art_movement = execute_query(AUTHOR_ARTMOVEMENT_QUERY, artist_name=author_name_dict)
    if not author_art_movement:
        author_art_movement = execute_query(AUTHOR_ARTMOVEMENT_QUERY, artist_name=author_name_dict,
                                            endpoint="http://fr.dbpedia.org/sparql")
    if not author_art_movement:
        author_art_movement = None
    else:
        author_art_movement = [r["art_movement"]["value"] for r in author_art_movement]
        author_art_movement = ", ".join(author_art_movement)
    return author_art_movement


def query_author_date(author_name_dict):
    author_birthDate = execute_query(AUTHOR_BIRTHDATE_QUERY, artist_name=author_name_dict)
    if not author_birthDate:
        author_birthDate = execute_query(AUTHOR_BIRTHDATE_QUERY, artist_name=author_name_dict,
                                         endpoint="http://fr.dbpedia.org/sparql")
    if not author_birthDate:
        author_birthDate = None
    else:
        author_birthDate = author_birthDate[0]["birthDate"]["value"]
    author_deathDate = execute_query(AUTHOR_DEATHDATE_QUERY, artist_name=author_name_dict)
    if not author_deathDate:
        author_deathDate = execute_query(AUTHOR_DEATHDATE_QUERY, artist_name=author_name_dict,
                                         endpoint="http://fr.dbpedia.org/sparql")
    if not author_deathDate:
        author_deathDate = None
    else:
        author_deathDate = author_deathDate[0]["deathDate"]["value"]
    return author_birthDate, author_deathDate


def query_author_place(author_name_dict):
    author_birthPlace = execute_query(AUTHOR_BIRTHPLACE_QUERY, artist_name=author_name_dict)
    if not author_birthPlace:
        author_birthPlace = execute_query(AUTHOR_BIRTHPLACE_QUERY, artist_name=author_name_dict,
                                          endpoint="http://fr.dbpedia.org/sparql")
    if not author_birthPlace:
        author_birthPlace = None
    else:
        author_birthPlace = [r["birthPlaceName"]["value"] for r in author_birthPlace]
        author_birthPlace = ", ".join(author_birthPlace)
    author_deathPlace = execute_query(AUTHOR_DEATHPLACE_QUERY, artist_name=author_name_dict)
    if not author_deathPlace:
        author_deathPlace = execute_query(AUTHOR_DEATHPLACE_QUERY, artist_name=author_name_dict,
                                          endpoint="http://fr.dbpedia.org/sparql")
    if not author_deathPlace:
        author_deathPlace = None
    else:
        author_deathPlace = [r["deathPlaceName"]["value"] for r in author_deathPlace]
        author_deathPlace = ", ".join(author_deathPlace)
    return author_birthPlace, author_deathPlace


def query_author_nationality(author_name_dict):
    author_nationality = execute_query(AUTHOR_NATIONALITY_QUERY, artist_name=author_name_dict)
    if not author_nationality:
        author_nationality = execute_query(AUTHOR_NATIONALITY_QUERY, artist_name=author_name_dict,
                                           endpoint="http://fr.dbpedia.org/sparql")
    if not author_nationality:
        author_nationality = None
    else:
        author_nationality = author_nationality[0]["nationality"]["value"]
    return author_nationality


def query_author_picture(author_name_dict):
    author_picture = execute_query(ARTIST_PICTURE_QUERY, artist_name=author_name_dict)
    if not author_picture:
        author_picture = execute_query(ARTIST_PICTURE_QUERY, artist_name=author_name_dict,
                                       endpoint="http://fr.dbpedia.org/sparql")
    if not author_picture:
        author_picture = None
    else:
        author_picture = author_picture[0]["picture"]["value"]
    return author_picture


def query_author_influence(author_name_dict):
    author_influence_en = execute_query(ARTIST_INFLUENCE_QUERY, artist_name=author_name_dict)
    author_influence_fr = execute_query(ARTIST_INFLUENCE_QUERY, artist_name=author_name_dict,
                                        endpoint="http://fr.dbpedia.org/sparql")
    author_influence_en = [r["authorName"]["value"] for r in author_influence_en]
    author_influence_fr = [r["authorName"]["value"] for r in author_influence_fr]
    list_author_influence = list(set().union(author_influence_en, author_influence_fr))
    list_author_influence = sorted(list_author_influence)
    if not list_author_influence:
        list_author_influence = None
    return list_author_influence


def query_author_influence_by(author_name_dict):
    author_influence_by_en = execute_query(ARTIST_INFLUENCE_BY_QUERY, artist_name=author_name_dict)
    author_influence_by_fr = execute_query(ARTIST_INFLUENCE_BY_QUERY, artist_name=author_name_dict,
                                           endpoint="http://fr.dbpedia.org/sparql")
    author_influence_by_en = [r["authorName"]["value"] for r in author_influence_by_en]
    author_influence_by_fr = [r["authorName"]["value"] for r in author_influence_by_fr]
    list_author_influence_by = list(set().union(author_influence_by_en, author_influence_by_fr))
    list_author_influence_by = sorted(list_author_influence_by)
    if not list_author_influence_by:
        list_author_influence_by = None
    return list_author_influence_by


def info_artist_from_name(author_name_dict):
    author_abstract = query_author_abstract(author_name_dict)
    author_gender = query_author_gender(author_name_dict)
    author_picture = query_author_picture(author_name_dict)
    list_author_influence_by = query_author_influence_by(author_name_dict)
    list_author_influence = query_author_influence(author_name_dict)
    author_art_movement = query_art_mov_author(author_name_dict)
    author_birthDate, author_deathDate = query_author_date(author_name_dict)
    author_birthPlace, author_deathPlace = query_author_place(author_name_dict)
    author_nationality = query_author_nationality(author_name_dict)
    json_dict = {"artist": author_name_dict,
                 "abstract": author_abstract,
                 "gender": author_gender,
                 "picture": author_picture,
                 "influenced by": list_author_influence_by,
                 "influence": list_author_influence,
                 "art movement": author_art_movement,
                 "birthDate": author_birthDate,
                 "birthPlace": author_birthPlace,
                 "deathDate": author_deathDate,
                 "deathPlace": author_deathPlace,
                 "nationality": author_nationality
                 }
    return json_dict

def query_artwork_list(artwork, artwork_french_name):
    author_name_dict = query_author_name(artwork, artwork_french_name)
    list_artwork_dbpedia_en = execute_query(ARTWORK_AUTHOR_LIST_QUERY, artist_name=author_name_dict)
    list_artwork_dbpedia_fr = execute_query(ARTWORK_AUTHOR_LIST_QUERY, artist_name=author_name_dict,
                                            endpoint="http://fr.dbpedia.org/sparql")
    list_artwork_dbpedia_en = [r["artworkName"]["value"] for r in list_artwork_dbpedia_en]
    list_artwork_dbpedia_fr = [r["artworkName"]["value"] for r in list_artwork_dbpedia_fr]
    list_artwork_same_author = list(set().union(list_artwork_dbpedia_en, list_artwork_dbpedia_fr))
    list_artwork_same_author = sorted(list_artwork_same_author)
    try:
        list_artwork_same_author.remove(artwork)
    except Exception:
        pass
    if not list_artwork_same_author:
        list_artwork_same_author = None
    return list_artwork_same_author


def query_artwork_dimension(artwork, artwork_french_name):
    artwork_dimension = execute_query(ARTWORK_SIZE_QUERY, artwork=artwork)
    if not artwork_dimension and artwork_french_name is not None:
        artwork_dimension = execute_query(ARTWORK_SIZE_QUERY, artwork=artwork_french_name, lang="fr",
                                          endpoint="http://fr.dbpedia.org/sparql")
    if not artwork_dimension:
        artwork_dimension_dict = None
    else:
        artwork_dimension_dict = {key: i[key]["value"] for i in artwork_dimension for key in i}
        artwork_dimension_dict = "x".join(artwork_dimension_dict.values())
    return artwork_dimension_dict


def query_artwork_useful_links(artwork, artwork_french_name):
    list_artwork_links_en = execute_query(ARTWORK_USEFUL_LINK_QUERY, artwork=artwork)
    if artwork_french_name is not None:
        list_artwork_links_fr = execute_query(ARTWORK_USEFUL_LINK_QUERY, artwork=artwork_french_name, lang="fr",
                                              endpoint="http://fr.dbpedia.org/sparql")
    else:
        list_artwork_links_fr = []
    list_artwork_links_en = [r["link"]["value"] for r in list_artwork_links_en]
    list_artwork_links_fr = [r["link"]["value"] for r in list_artwork_links_fr]
    list_artwork_links = list(set().union(list_artwork_links_en, list_artwork_links_fr))
    list_artwork_links = sorted(list_artwork_links)
    if not list_artwork_links:
        list_artwork_links = None
    return list_artwork_links


def query_advanced_info_artwork(artwork):
    artwork_french_name = query_artwork_french_name(artwork)
    list_artwork_same_author = query_artwork_list(artwork, artwork_french_name)
    artwork_dimension_dict = query_artwork_dimension(artwork, artwork_french_name)
    list_artwork_links = query_artwork_useful_links(artwork, artwork_french_name)
    json_dict = {"List artwork same author": list_artwork_same_author,
                 "artwork dimension": artwork_dimension_dict,
                 "artwork links": list_artwork_links
                 }
    return json_dict


def rangeYearCentury(year):
    try:
        year = int(year)
        century = (year - 1) // 100 + 1
        range = [(century - 1) * 100 + 1, century * 100]
        return range
    except:
        return None
