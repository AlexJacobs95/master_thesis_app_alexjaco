"""
In this file, it contains all the queries in SPARQL.
These predifined queries are used to do the final queries.
"""

QUERY_HEADER = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix dbo:  <http://dbpedia.org/ontology/>
prefix owl:  <http://www.w3.org/2002/07/owl#>
prefix prov: <http://www.w3.org/ns/prov#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix dbp:  <http://dbpedia.org/property/>
prefix dbp-fr:  <http://fr.dbpedia.org/property/>
prefix dct:  <http://purl.org/dc/terms/>
prefix yago: <http://dbpedia.org/class/yago/>
prefix umbel-rc: <http://umbel.org/umbel/rc/>
"""

RESOURCE_QUERY="""
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res rdfs:label <artwork_name>@LANG .
"""

ARTWORK_FR_NAME_QUERY ="""Select Distinct ?french_name_artwork 
Where {"""+RESOURCE_QUERY+"""
    ?res rdfs:label ?french_name_artwork.
    FILTER (lang(?french_name_artwork) = 'fr')
}"""

AUTHOR_NAME_QUERY ="""Select Distinct ?author_name ?good_author_name
Where {"""+RESOURCE_QUERY+"""
    ?res dbo:author ?author.
    ?author rdfs:label ?author_name.
    OPTIONAL{
        ?author dbo:wikiPageRedirects ?redirection.
        ?redirection rdfs:label ?good_author_name.
        FILTER (lang(?good_author_name) = 'en')
    }
    FILTER (lang(?author_name) = 'en')
}"""

ABSTRACT_QUERY = """Select Distinct ?abstract 
Where {"""+RESOURCE_QUERY+"""
    ?res dbo:abstract ?abstract.
    Filter (lang(?abstract) = 'en')
}"""

ARTWORK_YEAR_QUERY = """Select Distinct ?year 
Where {"""+RESOURCE_QUERY+"""
    ?res dbp:year ?year.
}"""

ARTWORK_YEAR_QUERY_FR = """Select Distinct ?year 
Where {"""+RESOURCE_QUERY+"""
    ?res dbp-fr:année ?year.
}"""


AUTHOR_GENDER_QUERY = """Select Distinct ?gender
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author foaf:gender ?gender.
}
"""

AUTHOR_ABSTRACT_QUERY ="""Select Distinct ?abstract
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:abstract ?abstract.
    Filter (lang(?abstract) = 'en')
}
"""

AUTHOR_BIRTHDATE_QUERY = """Select Distinct ?birthDate
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:birthDate ?birthDate.
}
"""
AUTHOR_DEATHDATE_QUERY = """Select Distinct ?deathDate
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:deathDate ?deathDate.
}
"""

AUTHOR_BIRTHPLACE_QUERY = """Select Distinct ?birthPlaceName
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:birthPlace ?birthPlace.
    ?birthPlace rdfs:label ?birthPlaceName.
    FILTER (lang(?birthPlaceName) = 'en')
}
"""
AUTHOR_DEATHPLACE_QUERY = """Select Distinct ?deathPlaceName
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:deathPlace ?deathPlace.
    ?deathPlace rdfs:label ?deathPlaceName.
    FILTER (lang(?deathPlaceName) = 'en')
}
"""

AUTHOR_NATIONALITY_QUERY = """Select Distinct ?nationality 
Where {
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    ?author dbo:birthPlace ?birthPlace.
    ?country dbo:demonym ?nationality.
   {
       ?birthPlace dbo:wikiPageRedirects ?redirection.
       ?redirection dbo:country ?country.
   }
   union
   {
        ?birthPlace dbo:country ?country.
   }
   union
   {
        ?birthPlace dbp:country ?country_label.
        ?country dbp:country ?country_label.
        ?country rdf:type dbo:Country.
   }
}
"""

AUTHOR_ARTMOVEMENT_QUERY = """Select Distinct ?art_movement
Where{
    ?author rdf:type dbo:Person.
    ?author rdfs:label "artist_name"@en.
    {
    ?author dbp:movement ?art_movement_res.
    }
    Union
    {
    ?author dbo:movement ?art_movement_res.
    }
    ?art_movement_res rdfs:label ?art_movement.
    FILTER (lang(?art_movement) = 'en')
}
"""


ARTWORK_LIST_BASIC_QUERY = """
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbo:author ?author.
    ?res rdfs:label ?artworkName.
    ?author rdfs:label "artist_name"@en.
    FILTER (lang(?artworkName) = 'en').
"""

ARTWORK_AUTHOR_LIST_QUERY ="""Select Distinct ?artworkName
Where{"""+ARTWORK_LIST_BASIC_QUERY+"""
}
"""

ARTWORK_MUSEUM_QUERY ="""Select Distinct ?museum
Where{"""+RESOURCE_QUERY+"""
    {
        ?res dbo:museum ?m.
        ?m rdfs:label ?museum.
        FILTER (lang(?museum) = 'en')
    }
    Union
    {?res dbp:museum ?museum.}
    Union
    {
         ?res dbp-fr:musée ?m.
         ?m rdfs:label ?museum.
         FILTER (lang(?museum) = 'en').
    }
}
"""

ARTWORK_MATERIAL_QUERY="""Select Distinct ?materialName
Where {"""+RESOURCE_QUERY+"""
    ?res dbp:material ?material.
    ?material rdfs:label ?materialName.
    FILTER (lang(?materialName) = 'en').
}
"""

ARTWORK_TYPE_QUERY="""Select Distinct ?typeName
Where {"""+RESOURCE_QUERY+"""
    {
        ?res dbp:type ?type.
        ?type rdfs:label ?typeName.
        FILTER (lang(?typeName) = 'en').
    }
    Union
    {
        ?res dbo:technique ?typeName.
    }
}
"""

ARTIST_INFLUENCE_QUERY = """Select Distinct ?authorName
Where{
    ?resAuthor rdf:type dbo:Person.
    ?resAuthor rdfs:label "artist_name"@en.
    ?author rdf:type dbo:Person.
    ?author dbo:influencedBy ?resAuthor.
    ?author rdfs:label ?authorName.
    FILTER (lang(?authorName) = 'en').
}
"""

ARTIST_INFLUENCE_BY_QUERY="""Select Distinct ?authorName
Where{
    ?resAuthor rdf:type dbo:Person.
    ?resAuthor rdfs:label "artist_name"@en.
    ?author rdf:type dbo:Person.
    ?author dbo:influenced ?resAuthor.
    ?author rdfs:label ?authorName.
    FILTER (lang(?authorName) = 'en').
}
"""

ARTIST_PICTURE_QUERY="""Select Distinct ?picture
Where{
    ?res rdf:type dbo:Person.
    ?res rdfs:label "artist_name"@en.
    ?res dbo:thumbnail ?picture.
}
"""

ARTWORK_PICTURE_QUERY="""Select Distinct ?picture
Where{"""+RESOURCE_QUERY+"""
    ?res dbo:thumbnail ?picture.
}
"""

ARTWORK_USEFUL_LINK_QUERY ="""Select Distinct ?link
Where{"""+RESOURCE_QUERY+"""
    ?res dbo:wikiPageExternalLink ?link.
}
"""

ARTWORK_SIZE_QUERY="""Select Distinct ?width ?length ?height
Where{"""+RESOURCE_QUERY+"""
    {
        ?res dbp:widthMetric ?width.
        ?res dbp:heightMetric ?height.
    }
    Union
    {
        ?res dbp:heightMetric ?height.
        ?res dbp:widthMetric ?width.
        ?res dbp:lengthMetric ?length.
    }
    Union
    {
        ?res dbp-fr:hauteur ?height.
        ?res dbp-fr:largeur ?width.
        ?res dbp-fr:profondeur ?length.
    }
    Union
    {
        ?res dbp-fr:hauteur ?height.
        ?res dbp-fr:largeur ?width.
    }  
}
"""

ARTWORK_LIST_MUSEUM_QUERY="""Select Distinct ?artworkName
Where{"""+ARTWORK_LIST_BASIC_QUERY+"""
    {
        ?res dbo:museum ?m.
        ?m rdfs:label ?museum.
        FILTER (lang(?museum) = 'en')
    }
    Union
    {?res dbp:museum ?museum.}
    Union
    {
     ?res dbp-fr:musée ?m.
     ?m rdfs:label ?museum.
     FILTER (lang(?museum) = 'en').
    }
    Filter(Contains(STR(?museum), "museum_name"))           
}
"""

ARTWORK_LIST_SAME_CENTURY = """Select Distinct ?artworkName
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    {?res dbp:year ?year.}
    Union
    {?res dbp-fr:année ?year.}
    ?res rdfs:label ?artworkName.
    Filter(lang(?artworkName) = 'en').
    Filter(?year >= xsd:integer(YEAR_START) && ?year <= xsd:integer(YEAR_END)).
}
"""

CHECK_ARTWORK = """Select Distinct ?res ?artworkName
Where{""" + RESOURCE_QUERY + """
    ?res rdfs:label ?artworkName.
    Filter(lang(?artworkName)='en').
}
"""

SEARCH_ART_MOVEMENT_QUERY = """Select Distinct ?mov_res ?value
Where{
    ?author rdf:type dbo:Person.
    ?author dbo:movement ?mov_res.
    ?mov_res rdfs:label ?value.
    filter(lang(?value)="en").
    filter contains(lcase(?value), lcase("search_data")).
}
"""

SEARCH_MUSEUM_QUERY = """Select Distinct ?mus ?value
Where{
    ?mus rdf:type dbo:Museum.
    ?mus rdfs:label ?value.
    filter(lang(?value)="en").
    filter contains(lcase(?value), lcase("search_data")).
}
"""

SEARCH_ARTWORK_LIST_MUSEUM_QUERY = """Select Distinct ?res ?artworkName ?author ?artistName ?mus ?value
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbo:author ?author.
    ?author rdfs:label ?artistName.
    filter(lang(?artistName)="en").
    ?res dbo:museum ?mus.
    ?mus rdf:type dbo:Museum.
    ?mus rdfs:label ?value.
    ?res rdfs:label ?artworkName.
    filter(lang(?artworkName)="en").
    filter(lang(?value)="en").
    filter contains(lcase(?value), lcase("search_data")).              
}
"""

SEARCH_TYPE_QUERY = """Select Distinct ?type ?type_value
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbp:type ?type.
    ?type rdfs:label ?type_value.
    filter(lang(?type_value)="en").
    filter contains(lcase(?type_value), lcase("search_data")).
    }
"""

SEARCH_ARTWORK_TYPE_QUERY = """Select Distinct ?type ?type_value ?res ?artworkName
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbp:type ?type.
    ?res rdfs:label ?artworkName.
    ?type rdfs:label ?type_value.
    filter(lang(?artworkName)="en").
    filter(lang(?type_value)="en").
    filter contains(lcase(?type_value), lcase("search_data")).
    }
"""

SEARCH_MATERIAL_QUERY = """Select Distinct ?mat ?mat_value
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbp:material ?mat.
    ?mat rdfs:label ?mat_value.
    filter(lang(?mat_value)="en").
    filter contains(lcase(?mat_value), lcase("search_data")).
    }
"""

SEARCH_ARTWORK_MATERIAL_QUERY = """Select Distinct ?mat ?mat_value ?res ?artworkName
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    ?res dbp:material ?mat.
    ?res rdfs:label ?artworkName.
    ?mat rdfs:label ?mat_value.
    filter(lang(?artworkName)="en").
    filter(lang(?mat_value)="en").
    filter contains(lcase(?mat_value), lcase("search_data")).
    }
"""

SEARCH_ARTIST_QUERY = """Select Distinct ?res ?value
Where{
    {?res rdf:type dbo:Artist.}
    Union{
    ?res rdf:type umbel-rc:Artist.}
    ?res rdfs:label ?value.
    filter(lang(?value)="en").
    filter contains(lcase(?value), lcase("search_data")).
}
"""

SEARCH_ARTWORK_ARTIST = """Select Distinct ?artist ?artistName ?res ?artworkName
Where{
    {
        ?res rdf:type dbo:Artwork.
    }
    Union
    {
        ?res rdf:type yago:Creation103129123.
    }
    {
        ?artist rdf:type dbo:Artist.
    }
    Union {
        ?artist rdf:type dbo:Person.
    }
    ?artist rdfs:label ?artistName.
    ?res dbo:author ?artist.
    ?res rdfs:label ?artworkName.
    filter(lang(?artistName)="en").
    filter contains(lcase(?artistName), lcase("search_data")).
    FILTER (lang(?artworkName) = 'en').
}
"""

SEARCH_ARTWORK_QUERY = """Select Distinct ?res ?value
Where{
    {
    ?res rdf:type dbo:Artwork.
    }
    Union
    {
    ?res rdf:type yago:Creation103129123.
    }
    ?res rdfs:label ?value.
    filter(lang(?value)="en").
    filter contains(lcase(?value), lcase("search_data")).
}
"""
