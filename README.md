# Master Thesis Application VUB 2019-2020
The repository hosts all produced code during my 2020 Master thesis in Web and Information Systems, 
done at the WISE lab of the Vrije Universiteit Brussel (VUB).
Master's thesis title: An Augmented Reality Museum Guide<br>
Promotor: Prof. Beat Signer<br>
Subject: Use of augmented reality for providing customised 
supplemental information in museum settings.<br>
***&copy; Vrije Universiteit Brussel, all rights reserved.***


## Installation Guide

This step-by-step installation guide is done in such way that after one 
completes all the following steps, one will be able to be run our server locally
on a computer running under a Linux distribution as operating system.
<br>
To run our application, you
will need the following requirements:<br>
  * Python 3.8
  * Flask
  * PostgreSQL 12
  * Pillow
  * SPARQLWrapper
  
The first step consist of configuring your terminal with environments variables, 
after downloading the code from the Gitlab repository. To do that, we
will use autoenv, it helps to execute a list of commands every time we access
a specific directory from a terminal. In order to that, you just need 
to excute the following commands-line in a terminal at the root application code
directory: 
```
echo "source ‘which activate.sh‘" » ~/.bashrc 
source ~/.bashrc
```

The second step, it is to check if all python package requirements are
already installed. For that open a terminal where the application code is
stored, and then you have to run the following command-line ```pip install -r
requirements.txt```

The last step of our application is to install the latest version of 
PostgreSQL. Once it is done, you just need to create the database. To do that,
you need to run this command-line in a terminal ```psql``` which open a 
PostgreSQL console, then types ```create database thesis_alexjaco_app``` 
Once completed, you will need to create the structure of our database by doing those
command-lines 
```
python manage.py db init 
python manage.py db migrate
python manage.py db upgrade
``` 
After that, you will need to fill our database
with predefined content by doing this ```python database_filler.py```

After all those steps, you can run this command-line ```python app.py```. This
starts our application server, and makes available our web-based application
at a localhost address.