"""
This file contains all the configuration needed for our server.
"""

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'fKCFotj5r_aILf_tUGu82SDG7Vkti3-X1lfrxjgwLqRoUVUBZpnxAEtfViMCyyckzS5eVrwsBOSzRw8EPIZChw'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
