"""
This helps us to create the data model of our database.
"""

from app import db


class User(db.Model):
    __tablename__ = 'user'

    username = db.Column(db.String(), primary_key=True)
    password = db.Column(db.String(), nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<id {}>'.format(self.username)


class ArtworkMarkerID(db.Model):
    __tablename__ = 'artworkmarkerid'

    identifier = db.Column(db.Integer, primary_key=True)
    artwork_name = db.Column(db.String(), nullable=False)
    artwork_art_movement = db.Column(db.String(), nullable=False)
    artwork_presence = db.Column(db.Integer, nullable=False)

    def __init__(self, identifier, artwork_name, artwork_art_movement, artwork_presence=1):
        self.identifier = identifier
        self.artwork_name = artwork_name
        self.artwork_art_movement = artwork_art_movement
        self.artwork_presence = artwork_presence

    def __repr__(self):
        return '<id {}>'.format(self.identifier)


class ArtworkRequestHistory(db.Model):
    __tablename__ = "artworkrequesthistory"

    artwork_name = db.Column(db.String(), primary_key=True)
    username = db.Column(db.String(), primary_key=True)

    def __init__(self, username, artwork_name):
        self.artwork_name = artwork_name
        self.username = username

    def __repr__(self):
        return '<artwork name {}>'.format(self.artwork_name)


class CustomSearchHistory(db.Model):
    __tablename__ = "customsearchhistory"

    search = db.Column(db.String(), primary_key=True)
    username = db.Column(db.String(), primary_key=True)
    parameters = db.Column(db.String(), primary_key=True)
    def __init__(self, username, search, parameters):
        self.search = search
        self.username = username
        self.parameters = parameters

    def __repr__(self):
        return '<search {}>'.format(self.search)
