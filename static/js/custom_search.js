/*
* In this javascript file, it contains all the code needed to create on the fly
* the datable witht the search and also to update it when it's requested.
* It contains function to request information about the keyword entered and the type of keywords
* and function to redirect to other application functionalities
*/


var checkboxToAvoid = "checkbox1";

if (data_query === null) {
    createSection("Search Results");
    table = document.createElement("table");
    table.setAttribute("id", "result_table");
    table.setAttribute("class", "display ");
    document.getElementById('table_container_Search Results').appendChild(table);

    var data = [];

    $('#result_table').DataTable({
        data: data,
        searching: false,
        lengthChange: false,
        columns: [
            {title: "Result Title", data: "res_title"},
            {title: "Result Link", data: "res_link"}
        ]
    });
} else {
    document.getElementById(data_query[1]).checked = true;
    var list_checkbox = document.getElementsByTagName("input");
    var i;
    for (i = 0; i < list_checkbox.length; i++) {
        if (list_checkbox[i].id === checkboxToAvoid && list_checkbox[i].value === data_query[0]) {
            list_checkbox[i].disabled = false;
            list_checkbox[i].checked = true;
            if (list_checkbox[i].value !== "no") {
                updateCheckBoxYesNo(document.getElementById(list_checkbox[i].id));
            }
        }
    }
    updateCheckBox(document.getElementById(data_query[1]));
    document.getElementById("search").value=data_query[2];
    processAndSendRequest();
    data_query = null;
}

function updateCheckBox(checkbox){
    if(checkbox.checked){
        makeUncheckableOtherCheckbox(checkbox);
    }else{
        makeCheckableAllCheckbox(checkbox);
    }
}

function updateCheckBoxYesNo(checkbox){
    if(checkbox.checked){
        var list_checkbox = document.getElementsByTagName("input");
        var i = 0;
        for(i = 0; i< list_checkbox.length; i++) {
            if (list_checkbox[i].id === checkboxToAvoid && list_checkbox[i].value !== checkbox.value) {
                list_checkbox[i].disabled = true;
                list_checkbox[i].checked = false;
            }
        }
    }else{
        var list_checkbox = document.getElementsByTagName("input");
        var i = 0;
        for(i = 0; i< list_checkbox.length; i++){
            if(list_checkbox[i].id === checkbox.id && list_checkbox[i].type === "checkbox"){
                list_checkbox[i].disabled = false;
            }
        }
    }
}

function makeUncheckableOtherCheckbox(checkbox) {
    var list_checkbox = document.getElementsByTagName("input");
    var i = 0;
    for(i = 0; i< list_checkbox.length; i++){
        if(list_checkbox[i].id !== checkboxToAvoid && list_checkbox[i].id !== checkbox.id && list_checkbox[i].type === "checkbox"){
            list_checkbox[i].disabled = true;
        }
        if(checkbox.id === "art_mov" && list_checkbox[i].id === checkboxToAvoid && list_checkbox[i].value === "yes"){
            list_checkbox[i].disabled = true;
        }
        if(checkbox.id === "art_mov" && list_checkbox[i].id === checkboxToAvoid && list_checkbox[i].value === "no"){
            list_checkbox[i].disabled = true;
            list_checkbox[i].checked = true;
        }
    }
}

function makeCheckableAllCheckbox(checkbox) {
    var list_checkbox = document.getElementsByTagName("input");
    var i = 0;
    for(i = 0; i< list_checkbox.length; i++){
        if(list_checkbox[i].id !== checkbox.id && list_checkbox[i].type === "checkbox"){
            list_checkbox[i].disabled = false;
        }
    }
}

function processAndSendRequest() {
    var list_input = document.getElementsByTagName("input");
    var data = {};
    var i;
    for(i=0; i < list_input.length; i++){
        if( list_input[i].type === "checkbox" && list_input[i].checked){
            data[list_input[i].id] = list_input[i].value;
        } else if(list_input[i].type === "text"){
            data[list_input[i].id] = list_input[i].value;
        }
    }
    var data_keys = Object.keys(data);
    if (data_keys.length !== 3){
        alert("Error: Please Check on keyword type and only one response for the first question!")
    } else {
        console.log(data);
        $.post('/process_search', {
            data: JSON.stringify([data[data_keys[0]], data[data_keys[1]], data[data_keys[2]]])
        }).done(function (resFromServer) {
            displayResult(resFromServer, [data[data_keys[0]], data[data_keys[1]], data[data_keys[2]]]);
        }).fail(function () {
            console.log("failed")
        })
    }
}

function createSection(titre) {
console.log("hey", "table_container_" + titre);
var $element = $('#' + titre);
if (!$element.length) {

    var section = document.createElement("SECTION");
    section.setAttribute('id', titre);
    section.setAttribute('class', "blue");
    section.setAttribute('style', "margin:110px; width:auto; height: auto");
    div_container = document.createElement('div');
    div_row = document.createElement('div');
    div_row.setAttribute('id', 'div_row');
    div_text = document.createElement('div');
    div_text.setAttribute("class", "text-center table_container");
    div_text.setAttribute("id", "table_container_" + titre);


    h2 = document.createElement('h2');
    h2.setAttribute("class", "titre-section");
    h2.innerHTML = titre;

    div_text.appendChild(h2);
    div_row.appendChild(div_text);
    div_container.appendChild(div_row);
    section.appendChild(div_container);
    document.body.appendChild(section);
}
}


function displayResult(resultFromServer, dataSendToServer) {
    console.log(resultFromServer);
    var data = [];

    var element = document.getElementById("Search Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Art Movement Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Material Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Type Technique Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Museum Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Artist Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }
    element = document.getElementById("Artwork Results");
    if (element !== null) {
        element.parentNode.removeChild(element);
    }

    if (dataSendToServer[1] === "art_mov") {
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            let href_link = '<a href="' + resultFromServer[i]["mov_res"]["value"] + '" style="color:blue">' + resultFromServer[i]["mov_res"]["value"] + '</a>';
            var dict_resutl = {
                "mov_title": resultFromServer[i]["value"]["value"],
                "mov_res": href_link
            }
            data.push(dict_resutl);
        }

        createSection("Art Movement Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById("table_container_Art Movement Results").appendChild(table);
        $('#result_table').DataTable({
            data: data,
            searching: false,
            lengthChange: false,
            columns: [
                {title: "Art Movement", data: "mov_title"},
                {title: "Link", data: "mov_res"}
            ]
        });
    } else if(dataSendToServer[1] === "material"){
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            if(dataSendToServer[0] == "yes"){
                let href_link_artwork = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
                let href_link_mat = '<a href="' + resultFromServer[i]["mat"]["value"] + '" style="color:blue">' +resultFromServer[i]["mat"]["value"] + '</a>';
                var dict_resutl = {
                    "artwork_title": resultFromServer[i]["artworkName"]["value"],
                    "artwork_res": href_link_artwork,
                    "mat_title": resultFromServer[i]["mat_value"]["value"],
                    "mat_res": href_link_mat
                }
            } else{
                let href_link_mat = '<a href="' + resultFromServer[i]["mat"]["value"] + '" style="color:blue">' +resultFromServer[i]["mat"]["value"] + '</a>';
                var dict_resutl = {
                    "mat_title": resultFromServer[i]["mat_value"]["value"],
                    "mat_res": href_link_mat
                }
            }
            data.push(dict_resutl);
        }

        createSection("Material Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById('table_container_Material Results').appendChild(table);
        if(dataSendToServer[0] === "yes") {
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Artwork", data: "artwork_title"},
                    {title: "Artwork Link", data: "artwork_res"},
                    {title: "Material", data: "mat_title"},
                    {title: "Material Link", data: "mat_res"}
                ]
            });
        }else{
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Material", data: "mat_title"},
                    {title: "Material Link", data: "mat_res"}
                ]
            });
        }
    } else if(dataSendToServer[1] === "type_technique"){
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            if(dataSendToServer[0] == "yes"){
                let href_link_artwork = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
                let href_link_type = '<a href="' + resultFromServer[i]["type"]["value"] + '" style="color:blue">' +resultFromServer[i]["type"]["value"] + '</a>';
                var dict_resutl = {
                    "artwork_title": resultFromServer[i]["artworkName"]["value"],
                    "artwork_res": href_link_artwork,
                    "type_title": resultFromServer[i]["type_value"]["value"],
                    "type_res": href_link_type
                }
            } else{
                let href_link_type = '<a href="' + resultFromServer[i]["type"]["value"] + '" style="color:blue">' +resultFromServer[i]["type"]["value"] + '</a>';
                var dict_resutl = {
                    "type_title": resultFromServer[i]["type_value"]["value"],
                    "type_res": href_link_type
                }
            }
            data.push(dict_resutl);
        }

        createSection("Type Technique Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById('table_container_Type Technique Results').appendChild(table);
        if(dataSendToServer[0] === "yes") {
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Artwork", data: "artwork_title"},
                    {title: "Artwork Link", data: "artwork_res"},
                    {title: "Type/Technique", data: "type_title"},
                    {title: "Type/Technique Link", data: "type_res"}
                ]
            });
        }else{
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Type/Technique", data: "type_title"},
                    {title: "Type/Technique Link", data: "type_res"}
                ]
            });
        }
    } else if(dataSendToServer[1] === "museum"){
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            if(dataSendToServer[0] == "yes"){
                let href_link_artwork = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
                let href_link_mus = '<a href="' + resultFromServer[i]["mus"]["value"] + '" style="color:blue">' +resultFromServer[i]["mus"]["value"] + '</a>';
                var dict_resutl = {
                    "artwork_title": resultFromServer[i]["artworkName"]["value"],
                    "artwork_res": href_link_artwork,
                    "museum_title": resultFromServer[i]["value"]["value"],
                    "museum_res": href_link_mus
                }
            } else{
                let href_link_mus = '<a href="' + resultFromServer[i]["mus"]["value"] + '" style="color:blue">' +resultFromServer[i]["mus"]["value"] + '</a>';
                var dict_resutl = {
                    "museum_title": resultFromServer[i]["value"]["value"],
                    "museum_res": href_link_mus
                }
            }
            data.push(dict_resutl);
        }

        createSection("Museum Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById('table_container_Museum Results').appendChild(table);
        if(dataSendToServer[0] === "yes") {
            $('#result_table').DataTable({
                data: data,
                searching: false,
                scrollX: true,
                lengthChange: false,
                columns: [
                    {title: "Artwork", data: "artwork_title"},
                    {title: "Artwork Link", data: "artwork_res"},
                    {title: "Artist", data: "artist_title"},
                    {title: "Artist Link", data: "artist_res"},
                    {title: "Museum", data: "museum_title"},
                    {title: "Museum Link", data: "museum_res"}
                ]
            });
        }else{
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Museum", data: "museum_title"},
                    {title: "Museum Link", data: "museum_res"}
                ]
            });
        }
    } else if(dataSendToServer[1] === "artist"){
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            if(dataSendToServer[0] == "yes"){
                console.log(resultFromServer[i]);
                let href_link_artwork = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
                let href_link_artist = '<a href="' + resultFromServer[i]["artist"]["value"] + '" style="color:blue">' + resultFromServer[i]["artist"]["value"] + '</a>';
                var dict_resutl = {
                    "artwork_title": resultFromServer[i]["artworkName"]["value"],
                    "artwork_res": href_link_artwork,
                    "artist_title": resultFromServer[i]["artistName"]["value"],
                    "artist_res": href_link_artist
                }
            } else{
                let href_link_artist = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
                var dict_resutl = {
                    "artist_title": resultFromServer[i]["value"]["value"],
                    "artist_res": href_link_artist
                }
            }
            data.push(dict_resutl);
        }

        createSection("Artist Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById('table_container_Artist Results').appendChild(table);
        if(dataSendToServer[0] === "yes") {
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Artwork", data: "artwork_title"},
                    {title: "Artwork Link", data: "artwork_res"},
                    {title: "Artist", data: "artist_title"},
                    {title: "Artist Link", data: "artist_res"}
                ]
            });
        }else{
            $('#result_table').DataTable({
                data: data,
                searching: false,
                lengthChange: false,
                columns: [
                    {title: "Artist", data: "artist_title"},
                    {title: "Artist Link", data: "artist_res"}
                ]
            });
        }
    } else if(dataSendToServer[1] === "artwork"){
        var i;
        for (i = 0; i < resultFromServer.length; i++) {
            let href_link_artwork = '<a href="' + resultFromServer[i]["res"]["value"] + '" style="color:blue">' + resultFromServer[i]["res"]["value"] + '</a>';
            var dict_resutl = {
                "artwork_title": resultFromServer[i]["value"]["value"],
                "artwork_res": href_link_artwork
            }
            data.push(dict_resutl);
        }

        createSection("Artwork Results");
        table = document.createElement("table");
        table.setAttribute("id", "result_table");
        table.setAttribute("class", "display ");

        document.getElementById('table_container_Artwork Results').appendChild(table);
        $('#result_table').DataTable({
            data: data,
            searching: false,
            lengthChange: false,
            columns: [
                {title: "Artwork", data: "artwork_title"},
                {title: "Artwork Link", data: "artwork_res"}
            ]
        });
    }
}
