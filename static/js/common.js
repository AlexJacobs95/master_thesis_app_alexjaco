/*
* Code to handle the display of tutorial popup
* */

var modal = document.getElementById("tutorialModal");
var btn = document.getElementById("tutorial");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function () {
    modal.style.display = "block";
}
span.onclick = function () {
    modal.style.display = "none";
}
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}