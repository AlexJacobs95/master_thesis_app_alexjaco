/*
* In this javascript file, it contains all the code needed to create on the fly
* the 2D tree with the artwork information and also to update it when it's requested.
* It contains function to request information from the server
* and function to redirect to other application functionalities
*/
if (basic_info_artwork !== null) {
    if (Object.keys(basic_info_artwork).length !== 11) {
        $.post("/artwork_basic_other_info", {
            year: basic_info_artwork["year"]
        }).done(function (resFromServer) {
            console.log(resFromServer);
            basic_info_artwork["list artwork same century"] = resFromServer["list artwork same century"];
            $.post("/artwork_basic_other_info", {
                artwork_name: basic_info_artwork["title"],
                artwork_museum: basic_info_artwork["museum"],
                artist: basic_info_artwork["artist"]
            }).done(function (resFromServer) {
                console.log(resFromServer);
                basic_info_artwork["list artwork same author in same museum"] = resFromServer["list artwork same author in same museum"];

                basic_info_artist = null;
                console.log("ici dans artist");
                $.post('/artist_info', {
                    artwork_name: basic_info_artwork["title"]
                }).done(function (resFromServer) {
                    basic_info_artist = resFromServer;
                    advanced_info_artwork = null;
                    console.log("ici dans advanced");
                    $.post('/advanced_artwork_info', {
                        artwork_name: basic_info_artwork["title"]
                    }).done(function (resFromServer) {
                        advanced_info_artwork = resFromServer;
                        displayData();
                    }).fail(function () {
                        console.log("failed")
                    })
                }).fail(function () {
                    console.log("failed")
                })

            }).fail(function () {
                console.log("failed")
            })
        }).fail(function () {
            console.log("failed")
        })
    } else {
        displayData();
    }
}

function displayData() {
    alert("The request is done. You can explore the data.")
    element = document.getElementById("display_main_view");
    if (element !== null) {
        element.setAttribute("style", "display: inline-block");
    }
    var artwork_title = document.createElement('li');
    var span = document.createElement("span");
    span.className = "caret-perso";
    var text = document.createTextNode(basic_info_artwork["title"]);
    span.appendChild(text);
    artwork_title.appendChild(span);
    document.getElementById("first").appendChild(artwork_title);

    var ul = document.createElement('ul');
    ul.id = "artwork_title";
    ul.className = "nested-perso";
    artwork_title.appendChild(ul);

    var artwork_year = document.createElement("li");
    if (basic_info_artwork["year"] !== null) {
        artwork_year.innerHTML = "Year: " + basic_info_artwork["year"];
    } else {
        artwork_year.innerHTML = "Year is missing";
    }
    artwork_year.className = "one";
    ul.appendChild(artwork_year);

    var artwork_dim = document.createElement("li");
    artwork_dim.className = "one";
    artwork_dim.innerHTML = advanced_info_artwork["artwork dimension"] === null ? "Dimension is missing" : "Dimension: " + advanced_info_artwork["artwork dimension"];
    ul.appendChild(artwork_dim);

    var artwork_mat_type = document.createElement("li");
    artwork_mat_type.className = "one";
    if (basic_info_artwork["material type"] !== null) {
        text = "Material & Type: " + basic_info_artwork["material type"].join(", ")
    } else {
        text = "Material and Type are not available"
    }
    artwork_mat_type.innerHTML = text;
    ul.appendChild(artwork_mat_type);

    var artwork_art_movement = document.createElement('li');
    artwork_art_movement.className = "one";
    if (basic_info_artwork["art movement"] !== null) {
        artwork_art_movement.innerHTML = "Art Movement: " + basic_info_artwork["art movement"];
    } else {
        artwork_art_movement.innerHTML = "Art Movement is missing";
    }
    ul.appendChild(artwork_art_movement);

    var artwork_museum = document.createElement('li');
    artwork_museum.className = "one";
    if (basic_info_artwork["museum"] !== null) {
        artwork_museum.innerHTML = "Museum: " + basic_info_artwork["museum"];
    } else {
        artwork_museum.innerHTML = "Museum is missing";
    }
    ul.appendChild(artwork_museum);

    var artwork_description = document.createElement('li');
    artwork_description.className = "one";
    if (basic_info_artwork["abstract"] !== null) {
        var a = document.createElement("a");
        artwork_description.appendChild(a);
        a.style.color = "blue";
        a.addEventListener('click', showDescriptionHandler(basic_info_artwork["abstract"], "Artwork Description"));
        a.innerHTML = "Description: click to display it";
    } else {
        artwork_description.innerHTML = "Description is missing";
    }
    ul.appendChild(artwork_description);

    var artwork_picture = document.createElement('li');
    artwork_picture.className = "one";
    if (basic_info_artwork["artwork picture"] !== null) {
        var a = document.createElement("a");
        artwork_picture.appendChild(a);
        a.style.color = "blue";
        a.href = 'javascript:showImage("' + basic_info_artwork["artwork picture"] + '",' + '"Artwork Picture")';
        a.innerHTML = "Picture";
    } else {
        artwork_picture.innerHTML = "Picture is missing";
    }
    ul.appendChild(artwork_picture);

    var artist_name = document.createElement("li");
    text = document.createTextNode(basic_info_artist["artist"])
    span = document.createElement("span");
    span.className = "caret-perso";
    span.appendChild(text);
    artist_name.appendChild(span);
    ul.appendChild(artist_name);

    var artist_ul = document.createElement('ul');
    artist_ul.id = "artwork_artist";
    artist_ul.className = "nested-perso";
    artist_name.appendChild(artist_ul);

    addInfoArtist(artist_ul, basic_info_artist);

    var artwork_museum_list_li = document.createElement("li");
    ul.appendChild(artwork_museum_list_li);
    if (basic_info_artwork["list artwork same author in same museum"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List artwork same author in same museum"));
        var artwork_museum_list_ul = document.createElement("ul");
        artwork_museum_list_ul.className = "nested-perso";
        artwork_museum_list_li.appendChild(span);
        artwork_museum_list_li.appendChild(artwork_museum_list_ul);
        var museum = document.createElement("li");
        museum.className = "one";
        artwork_museum_list_ul.appendChild(museum);
        createDivResult("artwork_museum", museum);
        var dataM = [];
        for (let j = 0; j < basic_info_artwork["list artwork same author in same museum"].length; j++) {
            var href_link = 'javascript:sendArtworkRequest(basic_info_artwork, \'list artwork same author in same museum\',' + j.toString() + ')';
            var ref = '<a href="' + href_link + '" style="color:blue">' + basic_info_artwork["list artwork same author in same museum"][j] + '</a>'
            var d = {"artwork": ref};
            dataM.push(d);
        }
        $(document).ready(function () {
            $('#result_artwork_museum_table').DataTable({
                data: dataM,
                searching: false,
                columns: [{title: "Artwork", data: "artwork"}]
            });
        });
    } else {
        artwork_museum_list_li.className = "one";
        artwork_museum_list_li.innerHTML = "No ohter same artwork in the museum";
    }


    var artwork_artist_list_li = document.createElement("li");
    ul.appendChild(artwork_artist_list_li);
    if (advanced_info_artwork["List artwork same author"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List artwork of the same artist"));
        var artwork_artist_list_ul = document.createElement("ul");
        artwork_artist_list_ul.className = "nested-perso";
        artwork_artist_list_li.appendChild(span);
        artwork_artist_list_li.appendChild(artwork_artist_list_ul);
        var artwork_artist = document.createElement("li");
        artwork_artist.className = "one";
        artwork_artist_list_ul.appendChild(artwork_artist);
        var dataArtworkArtist = [];
        createDivResult("artwork_artist", artwork_artist);
        for (let j = 0; j < advanced_info_artwork["List artwork same author"].length; j++) {
            var a = document.createElement("a");
            var href_link = 'javascript:sendArtworkRequest(advanced_info_artwork, \'List artwork same author\',' +j.toString()+')';
            var ref = '<a href="' + href_link + '" style="color:blue">' + advanced_info_artwork["List artwork same author"][j] + '</a>'
            var d = {"artwork": ref};
            dataArtworkArtist.push(d);
        }
        $(document).ready(function () {
            $('#result_artwork_artist_table').DataTable({
                data: dataArtworkArtist,
                searching: false,
                columns: [{title: "Artwork", data: "artwork"}]
            });
        });
    } else {
        artwork_artist_list_li.className = "one";
        artwork_artist_list_li.innerHTML = "No ohter artwork of the same artist";
    }


    var artwork_same_century_list_li = document.createElement("li");
    ul.appendChild(artwork_same_century_list_li);
    if (basic_info_artwork["list artwork same century"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List artwork of the same century"));
        var artwork_same_century_list_ul = document.createElement("ul");
        artwork_same_century_list_ul.className = "nested-perso";
        artwork_same_century_list_li.appendChild(span);
        artwork_same_century_list_li.appendChild(artwork_same_century_list_ul);
        var artwork_century = document.createElement("li");
        artwork_century.className = "one";
        artwork_same_century_list_ul.appendChild(artwork_century);
        createDivResult("artwork_century", artwork_century);
        var dataCentury = [];
        for (let j = 0; j < basic_info_artwork["list artwork same century"].length; j++) {
            var href_link = 'javascript:sendArtworkRequest(basic_info_artwork, \'list artwork same century\',' +j.toString()+')';
            var ref = '<a href="' + href_link + '" style="color:blue">' + basic_info_artwork["list artwork same century"][j] + '</a>';
            var d = {"artwork": ref};
            dataCentury.push(d);
        }
        $(document).ready(function () {
            $('#result_artwork_century_table').DataTable({
                data: dataCentury,
                searching: false,
                columns: [{title: "Artwork", data: "artwork"}]
            });
        });
    } else {
        artwork_same_century_list_li.className = "one";
        artwork_same_century_list_li.innerHTML = "No ohter artwork of the same century";
    }

    var artwork_usefullinks_list_li = document.createElement("li");
    if (advanced_info_artwork["artwork links"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List of useful links"));
        var artwork_usefullinks_list_ul = document.createElement("ul");
        artwork_usefullinks_list_ul.className = "nested-perso";
        artwork_usefullinks_list_li.appendChild(span);
        artwork_usefullinks_list_li.appendChild(artwork_usefullinks_list_ul);
        var artwork_links = document.createElement("li");
        artwork_links.className = "one";
        createDivResult("artwork_links", artwork_links);
        artwork_usefullinks_list_ul.appendChild(artwork_links);
        var dataLinks = [];
        for (let j = 0; j < advanced_info_artwork["artwork links"].length; j++) {
            var href_link = advanced_info_artwork["artwork links"][j];
            var ref = '<a href="' + href_link + '" style="color:blue">' + advanced_info_artwork["artwork links"][j] + '</a>';
            var d = {"link": ref};
            dataLinks.push(d);
        }
        $(document).ready(function () {
            $('#result_artwork_links_table').DataTable({
                data: dataLinks,
                searching: false,
                columns: [{title: "Link", data: "link"}]
            });
        });
    } else {
        artwork_usefullinks_list_li.className = "one";
        artwork_usefullinks_list_li.innerHTML = "No useful links";
    }
    ul.appendChild(artwork_usefullinks_list_li);

    expend();
    setInterval(expend, 5000);
}
function expend() {
    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
            this.parentElement.querySelector(".nested-perso").classList.toggle("active-perso");
            this.classList.toggle("caret-perso-down");
        });
    }
}

function createDivResult(title, element) {
    section = document.createElement("div");
    section.setAttribute('id', "titre");
    section.setAttribute('class', "blue");
    section.setAttribute('style', "width:65%; height: auto");
    div_container = document.createElement('div');
    div_row = document.createElement('div');
    div_row.setAttribute('id', 'div_row');
    div_text = document.createElement('div');
    div_text.setAttribute("class", "text-center table_container");
    div_text.setAttribute("id", "table_container_"+title);
    div_row.appendChild(div_text);
    div_container.appendChild(div_row);
    section.appendChild(div_container);
    element.appendChild(section);
    var table = document.createElement("table");
    table.setAttribute("id", "result_"+title+"_table");
    table.setAttribute("class", "display");
    div_text.appendChild(table);
}


function getArtistInfo(artist) {
    $.post('/info_artist', {
        data: artist
    }).done(function (resFromServer) {
        document.getElementById("second").innerHTML = "";
        var artist_name = document.createElement('li');
        var span = document.createElement("span");
        span.className = "caret-perso";
        var text = document.createTextNode(artist);
        span.appendChild(text);
        artist_name.appendChild(span);
        document.getElementById("second").appendChild(artist_name);
        var artist_ul = document.createElement('ul');
        artist_ul.id = "artist_name";
        artist_ul.className = "nested-perso";
        artist_name.appendChild(artist_ul);
        addInfoArtist(document.getElementById("artist_name"), resFromServer,"two");
        for (i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function () {
                this.parentElement.querySelector(".nested-perso").classList.toggle("active-perso");
                this.classList.toggle("caret-perso-down");
            });
        }
    }).fail(function () {
        console.log("failed")
    })
}

function addInfoArtist(artist_ul, dataToUse, section="first") {
    var artist_gender = document.createElement("li");
    artist_gender.className = "one";
    if (dataToUse["gender"] !== null) {
        text = "Gender: " + dataToUse["gender"];
    } else {
        text = "Gender is missing";
    }
    artist_gender.innerHTML = text;
    artist_ul.appendChild(artist_gender);

    var artist_nat = document.createElement("li");
    artist_nat.className = "one";
    if (dataToUse["nationality"] !== null) {
        text = "Nationality: " + dataToUse["nationality"];
    } else {
        text = "Nationality is missing";
    }
    artist_nat.innerHTML = text;
    artist_ul.appendChild(artist_nat);

    var artist_art_movement = document.createElement('li');
    artist_art_movement.className = "one";
    if (dataToUse["art movement"] !== null) {
        artist_art_movement.innerHTML = "Art Movement: " + dataToUse["art movement"];
    } else {
        artist_art_movement.innerHTML = "Art Movement is missing ";
    }
    artist_ul.appendChild(artist_art_movement);

    var artist_birthdate = document.createElement("li");
    artist_birthdate.className = "one";
    if (dataToUse["birthDate"] !== null) {
        text = "Birthdate: " + dataToUse["birthDate"];
    } else {
        text = "Birthdate is missing";
    }
    artist_birthdate.innerHTML = text;
    artist_ul.appendChild(artist_birthdate);

    var artist_deathdate = document.createElement("li");
    artist_deathdate.className = "one";
    if (dataToUse["deathDate"] !== null) {
        text = "Death date: " + dataToUse["deathDate"];
    } else {
        text = "Death date is missing";
    }
    artist_deathdate.innerHTML = text;
    artist_ul.appendChild(artist_deathdate);


    var artist_birthPlace = document.createElement("li");
    artist_birthPlace.className = "one";
    if (dataToUse["birthPlace"] !== null) {
        text = "Birth place: " + dataToUse["birthPlace"];
    } else {
        text = "Birth place is missing";
    }
    artist_birthPlace.innerHTML = text;
    artist_ul.appendChild(artist_birthPlace);

    var artist_deathPlace = document.createElement("li");
    artist_deathPlace.className = "one";
    if (dataToUse["deathPlace"] !== null) {
        text = "Death place: " + dataToUse["deathPlace"];
    } else {
        text = "Death place is missing";
    }
    artist_deathPlace.innerHTML = text;
    artist_ul.appendChild(artist_deathPlace);

    var artist_description = document.createElement('li');
    artist_description.className = "one";
    if (dataToUse["abstract"] !== null) {
        var a = document.createElement("a");
        a.innerHTML = "Description";
        a.style.color = "blue";
        var text = "Artist Description (" + dataToUse["artist"] + ")"
        a.addEventListener('click', showDescriptionHandler(dataToUse["abstract"], text));
        artist_description.appendChild(a);
    } else {
        artist_description.innerHTML = "Description is missing";
    }
    artist_ul.appendChild(artist_description);

    var artist_picture = document.createElement('li');
    artist_picture.className = "one";
    if (dataToUse["picture"] !== null) {
        var a = document.createElement("a");
        a.innerHTML = "Picture"
        a.style.color = "blue";
        var text = "Artist Picture (" + dataToUse["artist"] + ")"
        a.href = 'javascript:showImage("' + dataToUse["picture"] + '",' + '"' + text + '")';
        artist_picture.appendChild(a);
    } else {
        artist_picture.innerHTML = "Description is missing";
    }
    artist_ul.appendChild(artist_picture);

    var artist_influenced_by_li = document.createElement("li");
    artist_ul.appendChild(artist_influenced_by_li);
    if (dataToUse["influenced by"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List of People influenced by"))
        var artist_influenced_by_ul = document.createElement("ul");
        artist_influenced_by_ul.className = "nested-perso";
        artist_influenced_by_li.appendChild(span);
        artist_influenced_by_li.appendChild(artist_influenced_by_ul);
        var influenced_by = document.createElement("li");
        influenced_by.className = "one";
        artist_influenced_by_ul.appendChild(influenced_by);
        var title = "artist_influenced_by_"+section;
        createDivResult(title, influenced_by);
        var dataInfluencedBy = [];
        for (let j = 0; j < dataToUse["influenced by"].length; j++) {
            var href_link = "javascript:getArtistInfo('" + dataToUse["influenced by"][j] + "')";
            var ref = '<a href="'+href_link+'" style="color:blue">'+dataToUse["influenced by"][j]+'</a>';
            var d = {"artist": ref};
            dataInfluencedBy.push(d);
        }
        $(document).ready(function() {
            $('#result_'+title+'_table').DataTable({
                data: dataInfluencedBy,
                searching: false,
                columns: [{title: "Artist", data:"artist"}]
            });
        });
    } else {
        artist_influenced_by_li.className = "one";
        artist_influenced_by_li.innerHTML = "No people influenced by";
    }

    var artist_influence_li = document.createElement("li");
    artist_ul.appendChild(artist_influence_li);
    if (dataToUse["influence"] !== null) {
        span = document.createElement("span");
        span.className = "caret-perso";
        span.appendChild(document.createTextNode("List of People influence"))
        var artist_influence_ul = document.createElement("ul");
        artist_influence_ul.className = "nested-perso";
        artist_influence_li.appendChild(span);
        artist_influence_li.appendChild(artist_influence_ul);
        var influence = document.createElement("li");
        influence.className = "one";
        artist_influence_ul.appendChild(influence);
        var titleInfluence = "artist_influence_"+section;
        createDivResult(titleInfluence, influence);
        var dataInfluence = [];
        for (let j = 0; j < dataToUse["influence"].length; j++) {
            var href_link = "javascript:getArtistInfo('" + dataToUse["influence"][j] + "')";
            var ref ='<a href="'+href_link+'" style="color:blue">'+dataToUse["influence"][j]+'</a>';
            var d = {"artist": ref};
            dataInfluence.push(d);
        }
        $(document).ready(function() {
            $('#result_'+titleInfluence+'_table').DataTable({
                data: dataInfluence,
                searching: false,
                columns: [{title: "Artist", data:"artist"}]
            });
        });
    } else {
        artist_influence_li.className = "one";
        artist_influence_li.innerHTML = "No people influence";
    }
}

function showDescriptionHandler(dataToUse, title) {
    return function () {
        showDescription(dataToUse, title);
    };
}

function showDescription(dataToUse, title) {
    document.getElementById("info_area").value = "";
    document.getElementById("info_area").value = dataToUse;
    document.getElementById('info_area').style.display = "block";
    if (document.getElementById("image_area").style.display !== "none") {
        document.getElementById("image_area").style.display = "none";
    }
    var title_element = document.getElementById('info_title');
    title_element.innerHTML = "";
    title_element.style.display = "block";
    title_element.innerHTML = title;
}

function showImage(dataToUse, title) {
    var element = document.getElementById("image_area");
    element.src = "";
    element.src = dataToUse;
    element.style.display = "";
    if (document.getElementById("info_area").style.display !== "none") {
        document.getElementById("info_area").style.display = "none";
    }
    var title_element = document.getElementById('info_title');
    title_element.innerHTML = "";
    title_element.style.display = "block";
    title_element.innerHTML = title;
}

function sendArtworkRequest(data, key, value) {
    element = document.getElementById("display_main_view");
    if (element !== null) {
        element.display='none';
    }
    var form = document.createElement('form');
    form.setAttribute("action", "/data_vis");
    form.setAttribute("method", "post");
    var hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'artwork_name');
    hiddenField.setAttribute('value', data[key][value]);
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
}