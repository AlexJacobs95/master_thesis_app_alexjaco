/*
* In this javascript file, it contains all the code needed to create on the fly
* the augmented reality part and also to update it when it's requested.
* It contains function to request information from the server
* and function to redirect to other application functionalities
*/

let allow_clicks = false;
let numberOfPage = null;
let numberCurrentPage = null;
let keyword = null;

console.log(scan_marker);

if (basic_info_artwork !== null) {
    if (Object.keys(basic_info_artwork).length !== 11) {
        hideAllInfoSection();
        hideOtherButton();
        document.getElementById("scene").setAttribute("visible", "false");
        document.getElementById("functionality").setAttribute("visible", "false");
        document.getElementById("camera").setAttribute("visible", "false");
        $.post("/artwork_basic_other_info", {
            year: basic_info_artwork["year"]
        }).done(function (resFromServer) {
            console.log(resFromServer);
            basic_info_artwork["list artwork same century"] = resFromServer["list artwork same century"];
            $.post("/artwork_basic_other_info", {
                artwork_name: basic_info_artwork["title"],
                artwork_museum: basic_info_artwork["museum"],
                artist: basic_info_artwork["artist"]
            }).done(function (resFromServer) {
                console.log(resFromServer);
                basic_info_artwork["list artwork same author in same museum"] = resFromServer["list artwork same author in same museum"];

                basic_info_artist = null;
                console.log("ici dans artist");
                $.post('/artist_info', {
                    artwork_name: basic_info_artwork["title"]
                }).done(function (resFromServer) {
                    basic_info_artist = resFromServer;
                    advanced_info_artwork = null;
                    console.log("ici dans advanced");
                    $.post('/advanced_artwork_info', {
                        artwork_name: basic_info_artwork["title"]
                    }).done(function (resFromServer) {
                        advanced_info_artwork = resFromServer;
                        displayData();
                    }).fail(function () {
                        console.log("failed")
                    })
                }).fail(function () {
                    console.log("failed")
                })

            }).fail(function () {
                console.log("failed")
            })
        }).fail(function () {
            console.log("failed")
        })
    } else {
        displayData();
    }
} else if (lastArucoIdScan !== -1) {
    lastArucoIdScan = -1;
    scan_marker = true;
}

document.querySelector("#scene")
    .addEventListener('markerFound', (evt) => {
        allow_clicks = true
    })

document.querySelector("#scene")
    .addEventListener('markerLost', (evt) => {
        allow_clicks = false
    })

document.querySelector("#basic_info_artwork")
    .addEventListener('click', displayButtonBasicArtworkInfo);

document.querySelector("#basic_info_artist")
    .addEventListener('click', displayButtonBasicArtistInfo);

document.querySelector("#advanced_info_artwork")
    .addEventListener('click', displayButtonAdvancedArtworkInfo);

document.querySelector("#custom_search")
    .addEventListener('click', (evt) => {
        if (allow_clicks) {
            window.location.replace('/custom_search');
        }
    })

document.querySelector("#data_vis")
    .addEventListener('click', (evt) => {
        if (allow_clicks) {
            // window.location.replace('/data_vis');
            askDataVisView();
        }
    })

document.querySelector("#scan_artwork")
    .addEventListener('click', (evt) => {
        if (allow_clicks) {
            scan_marker = true;
            hideAllInfoSection();
            hideOtherButton();
            document.getElementById("scene").setAttribute("visible", "false");
            document.getElementById("functionality").setAttribute("visible", "false");
            document.getElementById("camera").setAttribute("visible", "false");
        }
    })

function displayArtworkArtistYearText() {
    var text = basic_info_artwork["title"] + "\n"
    if (basic_info_artwork["artist"] === null) {
        text = text + "Author is missing\n";
    } else {
        text = text + basic_info_artwork["artist"] + "\n";
    }
    if (basic_info_artwork["year"] === null) {
        text = text + "Year is missing";
    } else {
        text = text + basic_info_artwork["year"];
    }
    document.getElementById("artwork_artist_year_title").setAttribute("value", text);
    document.getElementById("artwork_artist_year_title").setAttribute("visible", "true");
}

function displayArtworkPicture() {
    var picture = '';
    if (basic_info_artwork["artwork picture"] !== null) {
        picture = "https://arjs-cors-proxy.herokuapp.com/" + basic_info_artwork["artwork picture"];
    } else {
        picture = "/static/img/no_picture_avalaible.png";
    }
    document.getElementById("artworkPicture").setAttribute("src", picture);
    document.getElementById("artworkPicture").setAttribute("visible", "true");
}

function displayArtworkInfo() {
    console.log("ici dans display artwork info");
    if (allow_clicks && document.getElementById("basic_info_artwork_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById('artwork_basic_info_area').setAttribute("visible", "true");
        var text = "Title: " + basic_info_artwork["title"] + "\n";
        if (basic_info_artwork["year"] === null) {
            text = text + "Year: Missing\n";
        } else {
            text = text + "Year: " + basic_info_artwork["year"] + "\n";
        }

        if (basic_info_artwork["material type"] === null) {
            text = text + "Material & Type: Missing\n";
        } else {
            text = text + "Material & Type: " + basic_info_artwork["material type"].join(", ") + "\n";
        }
        if (basic_info_artwork["art movement"] === null) {
            text = text + "Artwork Art Movement: Missing\n";
        } else {
            text = text + "Artwork Art Movement: " + basic_info_artwork["art movement"] + "\n";
        }

        if (advanced_info_artwork["artwork dimension"] === null) {
            text = text + "Dimension: Missing\n";
        } else {
            text = text + "Dimension: " + advanced_info_artwork["artwork dimension"] + "\n";
        }

        if (basic_info_artwork["museum"] === null) {
            text = text + "Museum: Missing\n";
        } else {
            text = text + "Museum: " + basic_info_artwork["museum"] + "\n";
        }

        if (basic_info_artist["artist"] === null) {
            text = text + "Author: Missing\n";
        } else {
            text = text + "\nArtist: " + basic_info_artist["artist"] + "\n";
        }
        if (basic_info_artist["birthDate"] === null) {
            text = text + "Birth date: Missing\n";
        } else {
            text = text + "Birth date: " + basic_info_artist["birthDate"] + "\n";
        }
        if (basic_info_artist["deathDate"] === null) {
            text = text + "Death date: Missing\n";
        } else {
            text = text + "Death date: " + basic_info_artist["deathDate"] + "\n";
        }
        if (basic_info_artist["nationality"] === null) {
            text = text + "Nationality: Missing\n";
        } else {
            text = text + "Nationality: " + basic_info_artist["nationality"] + "\n";
        }
        if (basic_info_artist["art movement"] === null) {
            text = text + "Art Movement: Missing\n";
        } else {
            text = text + "Art Movement: " + basic_info_artist["art movement"] + "\n";
        }
        document.getElementById("artwork_basic_info_area").setAttribute("value", text);
    }
}

function displayArtworkAbstract() {
    if (allow_clicks && document.getElementById("basic_info_artwork_button")) {
        hideAllInfoSection();
        if (basic_info_artwork["abstract"] !== null) {
            $.post('/process_abstract', {
                data: basic_info_artwork["abstract"],
                title: basic_info_artwork["title"],
                type: "artwork"
            }).done(function (resFromServer) {
                document.getElementById("artworkAbstract").setAttribute('visible', 'true');
                document.getElementById('artworkAbstract').setAttribute("src", resFromServer["path"]);
                var heighScale = 5 / (resFromServer["w"] / resFromServer["h"]);
                document.getElementById("artworkAbstract").setAttribute("scale", "5 " + heighScale.toString() + " 0")
            }).fail(function () {
                console.log("failed")
            })
        } else {
            document.getElementById("artwork_abstract_area").setAttribute("visible", "true");
        }
    }
}

function displayArtistInfo() {
    if (allow_clicks && document.getElementById("basic_info_artist_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById("artist_basic_info_area").setAttribute("visible", "true");
        var text = "";
        if (basic_info_artist["artist"] === null) {
            text = text + "Author: Missing\n";
        } else {
            text = text + "\nArtist: " + basic_info_artist["artist"] + "\n";
        }
        if (basic_info_artist["gender"] === null) {
            text = text + "Gender: Missing\n";
        } else {
            text = text + "Gender: " + basic_info_artist["gender"] + "\n";
        }
        if (basic_info_artist["nationality"] === null) {
            text = text + "Nationality: Missing\n";
        } else {
            text = text + "Nationality: " + basic_info_artist["nationality"] + "\n";
        }
        if (basic_info_artist["art movement"] === null) {
            text = text + "Art Movement: Missing\n";
        } else {
            text = text + "Art Movement: " + basic_info_artist["art movement"] + "\n";
        }
        if (basic_info_artist["birthDate"] === null) {
            text = text + "Birthdate: Missing\n";
        } else {
            text = text + "Birthdate: " + basic_info_artist["birthDate"] + "\n";
        }
        if (basic_info_artist["deathDate"] === null) {
            text = text + "Death date: Missing\n";
        } else {
            text = text + "Death date: " + basic_info_artist["deathDate"] + "\n";
        }
        if (basic_info_artist["birthPlace"] === null) {
            text = text + "Birth place: Missing\n";
        } else {
            text = text + "Birth place: " + basic_info_artist["birthPlace"] + "\n";
        }
        if (basic_info_artist["deathPlace"] === null) {
            text = text + "Death place: Missing\n";
        } else {
            text = text + "Death place: " + basic_info_artist["deathPlace"] + "\n";
        }
        document.getElementById("artist_basic_info_area").setAttribute("value", text);
    }
}

function displayArtistAbstract() {
    if (allow_clicks && document.getElementById("basic_info_artist_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        if (basic_info_artwork["abstract"] !== null) {
            $.post('/process_abstract', {
                data: basic_info_artist["abstract"],
                title: basic_info_artist["artist"],
                type: "artist"
            }).done(function (resFromServer) {
                document.getElementById("artistAbstract").setAttribute('visible', 'true');
                document.getElementById('artistAbstract').setAttribute("src", resFromServer["path"]);
                var heighScale = 5 / (resFromServer["w"] / resFromServer["h"]);
                document.getElementById("artistAbstract").setAttribute("scale", "5 " + heighScale.toString() + " 0")
            }).fail(function () {
                console.log("failed")
            })
        } else {
            document.getElementById("artist_abstract_area").setAttribute("visible", "true");
        }
    }
}

function displayArtistPicture() {
    if (allow_clicks && document.getElementById("basic_info_artist_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        var artist_picture = '';
        if (basic_info_artist["picture"] !== null) {
            artist_picture = "https://arjs-cors-proxy.herokuapp.com/" + basic_info_artist['picture'];
        } else {
            artist_picture = "/static/img/no_picture_avalaible.png";
        }
        console.log(artist_picture);
        document.getElementById("artistPicture").setAttribute("src", artist_picture);
        document.getElementById("artistPicture").setAttribute("visible", "true");
    }
}

function displayArtistInfluence() {
    if (allow_clicks && document.getElementById("basic_info_artist_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById('next_artist').setAttribute("visible", "false");
        document.getElementById('previous_artist').setAttribute("visible", "false");
        document.querySelector("#next_artist").removeEventListener('click', showNextPage);
        document.querySelector("#previous_artist").removeEventListener('click', showNextPage);
        var text = "";
        if (basic_info_artist["influence"] !== null) {
            var i = 0;
            numberOfPage = Math.ceil(basic_info_artist["influence"].length / 10);
            numberCurrentPage = 0;
            keyword = "influence";
            while (i < basic_info_artist["influence"].length && i < 10) {
                text = text + basic_info_artist["influence"][i] + "\n";
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next_artist').setAttribute("visible", "true");
                document.querySelector("#next_artist").addEventListener('click', showNextPage);
            }
        } else {
            text = "No people influence"
        }
        document.getElementById("list_artist").setAttribute('value', text);
        document.getElementById("list").setAttribute("visible", "true");
    }
}

function displayArtistInfluenceBy() {
    if (allow_clicks && document.getElementById("basic_info_artist_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById('next_artist').setAttribute("visible", "false");
        document.getElementById('previous_artist').setAttribute("visible", "false");
        document.querySelector("#next_artist").removeEventListener('click', showNextPage);
        document.querySelector("#previous_artist").removeEventListener('click', showNextPage);
        var text = "";
        if (basic_info_artist["influenced by"] !== null) {
            var i = 0;
            numberOfPage = Math.ceil(basic_info_artist["influenced by"].length / 10);
            numberCurrentPage = 0;
            keyword = "influenced by";
            while (i < basic_info_artist["influenced by"].length && i < 10) {
                text = text + basic_info_artist["influenced by"][i] + "\n";
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next_artist').setAttribute("visible", "true");
                document.querySelector("#next_artist").addEventListener('click', showNextPage);
            }
        } else {
            text = "No people influenced by"
        }
        document.getElementById("list_artist").setAttribute('value', text);
        document.getElementById("list").setAttribute("visible", "true");
    }
}

function displayArtworkMuseumInfo() {
    let id;
    if (allow_clicks && document.getElementById("advanced_info_artwork_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById("next").setAttribute("visible", "false");
        document.getElementById("previous").setAttribute("visible", "false");
        document.querySelector("#next").removeEventListener('click', showNextPage);
        document.querySelector("#previous").removeEventListener('click', showNextPage);
        let i = 0;
        while (i < 5) {
            id = "links_" + (i + 1).toString();
            document.getElementById(id).setAttribute("visible", "false");
            i++;
        }
        if (basic_info_artwork["list artwork same author in same museum"] !== null) {
            i = 0
            numberOfPage = Math.ceil(basic_info_artwork["list artwork same author in same museum"].length / 5);
            numberCurrentPage = 0;
            keyword = "museum";
            while (i < basic_info_artwork["list artwork same author in same museum"].length && i < 5) {
                id = "links_" + (i + 1).toString();
                document.getElementById(id).setAttribute("value",
                    basic_info_artwork["list artwork same author in same museum"][i]);
                document.getElementById(id).setAttribute("visible", "true");
                document.getElementById(id).addEventListener('click', showArtwork);
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next').setAttribute("visible", "true");
                document.querySelector("#next").addEventListener('click', showNextPage);
            }
            document.getElementById("real_links_area").setAttribute("visible", "true");
        } else {
            var text = "No ohter artwork\nin the same museum";
            document.getElementById("no_links_area").setAttribute("visible", "true");
            document.getElementById("no_links_area_text").setAttribute("value", text);
        }
        document.getElementById("real_links").setAttribute("visible", "true");
    }
}

function displayArtworkSameArtistInfo() {
    let id;
    if (allow_clicks && document.getElementById("advanced_info_artwork_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById("next").setAttribute("visible", "false");
        document.getElementById("previous").setAttribute("visible", "false");
        document.querySelector("#next").removeEventListener('click', showNextPage);
        document.querySelector("#previous").removeEventListener('click', showNextPage);
        let i = 0;
        while (i < 5) {
            id = "links_" + (i + 1).toString();
            document.getElementById(id).setAttribute("visible", "false");
            i++;
        }
        if (advanced_info_artwork["List artwork same author"] !== null) {
            i = 0
            numberOfPage = Math.ceil(advanced_info_artwork["List artwork same author"].length / 5);
            numberCurrentPage = 0;
            keyword = "artist";
            while (i < advanced_info_artwork["List artwork same author"].length && i < 5) {
                id = "links_" + (i + 1).toString();
                console.log(id);
                console.log(advanced_info_artwork["List artwork same author"][i]);
                document.getElementById(id).setAttribute("value",
                    advanced_info_artwork["List artwork same author"][i]);
                document.getElementById(id).setAttribute("visible", "true");
                document.getElementById(id).addEventListener('click', showArtwork);
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next').setAttribute("visible", "true");
                document.querySelector("#next").addEventListener('click', showNextPage);
            }
            document.getElementById("real_links_area").setAttribute("visible", "true");
        } else {
            let text = "No ohter artwork\nof the same artist";
            document.getElementById("no_links_area").setAttribute("visible", "true");
            document.getElementById("no_links_area_text").setAttribute("value", text);
        }
        document.getElementById("real_links").setAttribute("visible", "true");
    }
}

function displayArtworkSameCenturyInfo() {
    let id;
    if (allow_clicks && document.getElementById("advanced_info_artwork_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById("next").setAttribute("visible", "false");
        document.getElementById("previous").setAttribute("visible", "false");
        document.querySelector("#next").removeEventListener('click', showNextPage);
        document.querySelector("#previous").removeEventListener('click', showNextPage);
        let i = 0;
        while (i < 5) {
            id = "links_" + (i + 1).toString();
            document.getElementById(id).setAttribute("visible", "false");
            i++;
        }
        if (basic_info_artwork["list artwork same century"] !== null) {
            i = 0
            numberOfPage = Math.ceil(basic_info_artwork["list artwork same century"].length / 5);
            numberCurrentPage = 0;
            keyword = "century";
            while (i < basic_info_artwork["list artwork same century"].length && i < 5) {
                id = "links_" + (i + 1).toString();
                console.log(id);
                console.log(basic_info_artwork["list artwork same century"][i]);
                document.getElementById(id).setAttribute("value",
                    basic_info_artwork["list artwork same century"][i]);
                document.getElementById(id).setAttribute("visible", "true");
                document.getElementById(id).addEventListener('click', showArtwork);
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next').setAttribute("visible", "true");
                document.querySelector("#next").addEventListener('click', showNextPage);
            }
            document.getElementById("real_links_area").setAttribute("visible", "true");
        } else {
            let text = "No ohter artwork\nof the same century";
            document.getElementById("no_links_area").setAttribute("visible", "true");
            document.getElementById("no_links_area_text").setAttribute("value", text);
        }
        document.getElementById("real_links").setAttribute("visible", "true");
    }
}

function displayUsefulLinksInfo() {
    let id;
    if (allow_clicks && document.getElementById("advanced_info_artwork_button").getAttribute("visible") === true) {
        hideAllInfoSection();
        document.getElementById("next").setAttribute("visible", "false");
        document.getElementById("previous").setAttribute("visible", "false");
        document.querySelector("#next").removeEventListener('click', showNextPage);
        document.querySelector("#previous").removeEventListener('click', showNextPage);
        let i = 0;
        while (i < 5) {
            id = "links_" + (i + 1).toString();
            document.getElementById(id).setAttribute("visible", "false");
            i++;
        }
        if (advanced_info_artwork["artwork links"] !== null) {
            i = 0
            numberOfPage = Math.ceil(advanced_info_artwork["artwork links"].length / 5);
            numberCurrentPage = 0;
            keyword = "links";
            while (i < advanced_info_artwork["artwork links"].length && i < 5) {
                id = "links_" + (i + 1).toString();
                console.log(id);
                console.log(advanced_info_artwork["artwork links"][i]);
                document.getElementById(id).setAttribute("value",
                    advanced_info_artwork["artwork links"][i]);
                document.getElementById(id).setAttribute("visible", "true");
                document.getElementById(id).addEventListener('click', showLinks);
                i++;
            }
            if (numberOfPage > 1) {
                document.getElementById('next').setAttribute("visible", "true");
                document.querySelector("#next").addEventListener('click', showNextPage);
            }
            document.getElementById("real_links_area").setAttribute("visible", "true");
        } else {
            let text = "No ohter artwork\nof the same century";
            document.getElementById("no_links_area").setAttribute("visible", "true");
            document.getElementById("no_links_area_text").setAttribute("value", text);
        }
        document.getElementById("real_links").setAttribute("visible", "true");
    }
}

function showNextArtist(dataToUse) {
    console.log("dans showNextArtist");
    console.log(numberCurrentPage);
    let i = (numberCurrentPage + 1) * 10;
    let j = 0;
    let text = "";
    while (i < dataToUse.length && j < 10) {
        console.log(i);
        text = text + dataToUse[i] + "\n";
        i++;
        j++;
    }
    numberCurrentPage = numberCurrentPage + 1;
    console.log(numberCurrentPage);
    document.getElementById("previous_artist").setAttribute('visible', "true");
    document.getElementById("previous_artist").addEventListener('click', showPreviousPage);
    document.getElementById("list_artist").setAttribute('value', text);
    if (numberCurrentPage + 1 === numberOfPage) {
        document.getElementById('next_artist').setAttribute("visible", "false");
        document.getElementById('next_artist').removeEventListener('click', showNextPage);
    }
}

function showArtwork() {
    scan_marker = false;
    hideAllInfoSection();
    hideOtherButton();
    document.getElementById("scene").setAttribute("visible", "false");
    document.getElementById("functionality").setAttribute("visible", "false");
    document.getElementById("camera").setAttribute("visible", "false");
    $.post('/artwork_info', {
        artwork_name: document.getElementById(this.id).getAttribute("value")
    }).done(function (resFromServer) {
        console.log(resFromServer);
        basic_info_artwork = resFromServer;
        $.post("/artwork_basic_other_info", {
            year: basic_info_artwork["year"]
        }).done(function (resFromServer) {
            console.log(resFromServer);
            basic_info_artwork["list artwork same century"] = resFromServer["list artwork same century"];
            $.post("/artwork_basic_other_info", {
                artwork_name: basic_info_artwork["title"],
                artwork_museum: basic_info_artwork["museum"],
                artist: basic_info_artwork["artist"]
            }).done(function (resFromServer) {
                console.log(resFromServer);
                basic_info_artwork["list artwork same author in same museum"] = resFromServer["list artwork same author in same museum"];
                basic_info_artist = null;
                console.log("ici dans artist");
                $.post('/artist_info', {
                    artwork_name: basic_info_artwork["title"]
                }).done(function (resFromServer) {
                    console.log(resFromServer);
                    basic_info_artist = resFromServer;
                    console.log("ici dans advanced");
                    $.post('/advanced_artwork_info', {
                        artwork_name: basic_info_artwork["title"]
                    }).done(function (resFromServer) {
                        console.log(resFromServer);
                        advanced_info_artwork = resFromServer;
                        displayData();
                    }).fail(function () {
                        console.log("failed")
                    })
                }).fail(function () {
                    console.log("failed")
                })
            }).fail(function () {
                console.log("failed")
            })
        }).fail(function () {
            console.log("failed")
        })
    }).fail(function () {
        console.log("failed")
    })
}

function showLinks() {
    document.getElementById("functionality").setAttribute("visible", "false");
    document.getElementById("camera").setAttribute("visible", "false");
    window.location.href = document.getElementById(this.id).getAttribute("value");
}

function showNextArtwork(dataToUse) {
    console.log("dans showNextArtwork");
    console.log(numberCurrentPage);
    let i = (numberCurrentPage + 1) * 5;
    let j = 0;
    while (j < 5) {
        var id = "links_" + (j + 1).toString();
        document.getElementById(id).setAttribute("visible", "false");
        j++;
    }
    j = 0;
    while (i < dataToUse.length && j < 5) {
        var id = "links_" + (j + 1).toString();
        console.log(id);
        console.log(i);
        document.getElementById(id).setAttribute("value", dataToUse[i]);
        document.getElementById(id).setAttribute("visible", "true");
        i++;
        j++;
    }
    numberCurrentPage = numberCurrentPage + 1;
    document.getElementById("previous").setAttribute('visible', "true");
    document.getElementById("previous").addEventListener('click', showPreviousPage);
    if (numberCurrentPage + 1 === numberOfPage) {
        document.getElementById('next').setAttribute("visible", "false");
        document.getElementById('next').removeEventListener('click', showNextPage);
    }
}

function showNextPage() {
    if (allow_clicks) {
        if (keyword === "influence") {
            showNextArtist(basic_info_artist["influence"]);
        } else if (keyword === "influenced by") {
            showNextArtist(basic_info_artist["influenced by"]);
        } else if (keyword === "museum") {
            showNextArtwork(basic_info_artwork["list artwork same author in same museum"]);
        } else if (keyword === "artist") {
            showNextArtwork(advanced_info_artwork["List artwork same author"]);
        } else if (keyword === "century") {
            showNextArtwork(basic_info_artwork["list artwork same century"]);
        } else if (keyword === "links") {
            showNextArtwork(advanced_info_artwork["artwork links"]);
        }
    }
}

function showPreviousArtist(dataToUse) {
    let i;
    let text = "";
    if (numberCurrentPage === 1) {
        document.getElementById('previous_artist').setAttribute("visible", "false");
        document.getElementById('previous_artist').removeEventListener("click", showPreviousArtist);
        numberCurrentPage = 0;
        i = 0;
        while (i < dataToUse.length && i < 10) {
            text = text + dataToUse[i] + "\n";
            i++;
        }
        document.getElementById('next_artist').setAttribute("visible", "true");
        document.getElementById('next_artist').addEventListener('click', showNextPage);
    } else {
        i = (numberCurrentPage - 1) * 10;
        let j = 0;
        while (i < dataToUse.length && j < 10) {
            text = text + dataToUse[i] + "\n";
            i++;
            j++;
        }
        numberCurrentPage = numberCurrentPage - 1;
        document.getElementById("previous_artist").setAttribute('visible', "true");
        document.getElementById("previous_artist").addEventListener('click', showPreviousPage);
    }
    document.getElementById("list_artist").setAttribute('value', text);
}

function showPreviousArtwork(dataToUse) {
    let id;
    let i = 0;
    if (numberCurrentPage === 1) {
        document.getElementById('previous').setAttribute("visible", "false");
        document.getElementById('previous').removeEventListener("click", showPreviousArtist);
        numberCurrentPage = 0;
        while (i < dataToUse.length && i < 5) {
            id = "links_" + (i + 1).toString();
            document.getElementById(id).setAttribute("value", dataToUse[i]);
            document.getElementById(id).setAttribute("visible", "true");
            i++;
        }
        document.getElementById('next').setAttribute("visible", "true");
        document.querySelector("#next").addEventListener('click', showNextPage);
    } else {
        i = (numberCurrentPage - 1) * 5;
        let j = 0;
        while (i < dataToUse.length && j < 5) {
            id = "links_" + (j + 1).toString();
            document.getElementById(id).setAttribute("value", dataToUse[i]);
            document.getElementById(id).setAttribute("visible", "true");
            i++;
            j++;
        }
        numberCurrentPage = numberCurrentPage - 1;
        document.getElementById("previous").setAttribute('visible', "true");
        document.getElementById("previous").addEventListener('click', showPreviousPage);
    }
}

function showPreviousPage() {
    if (allow_clicks) {
        if (keyword === "influence") {
            showPreviousArtist(basic_info_artist["influence"]);
        } else if (keyword === "influenced by") {
            showPreviousArtist(basic_info_artist["influenced by"]);
        } else if (keyword === "museum") {
            showPreviousArtwork(basic_info_artwork["list artwork same author in same museum"]);
        } else if (keyword === "century") {
            showPreviousArtwork(basic_info_artwork["list artwork same century"]);
        } else if (keyword === "links") {
            showPreviousArtwork(advanced_info_artwork["artwork links"]);
        }
    }
}

function createButtonRelated(idButton, position, scale, idElement, functionHandler, modelPath) {
    var button = document.createElement("a-entity");
    button.setAttribute("position", position);
    button.setAttribute("id", idButton);
    button.setAttribute("rotation", "0 -90 0");
    button.setAttribute("scale", scale);
    button.setAttribute("gltf-model", "src: url(" + modelPath + ")");
    document.getElementById(idElement).appendChild(button);
    document.querySelector("#" + idButton)
        .addEventListener('click', functionHandler);
}

function displayButtonBasicArtworkInfo() {
    if (allow_clicks) {
        if (document.getElementById("basic_info_artwork_button").childElementCount === 0) {
            hideOtherButton();
            hideAllInfoSection();
            removeListenerAndButton("artwork");

            createButtonRelated("artwork_artist_info", "0 0 0",
                "5 5 5", "basic_info_artwork_button",
                displayArtworkInfo, "/static/3d_models/artwork_info_circle.gltf");

            createButtonRelated("artwork_abstract", "1 0 0",
                "5 5 5", "basic_info_artwork_button",
                displayArtworkAbstract, "/static/3d_models/artwork_abstract_circle.gltf");

            document.getElementById("button_related_to_functionality").setAttribute("visible", "true");
            document.getElementById('basic_info_artwork_button').setAttribute("visible", "true");
        }
    }
}

function displayButtonBasicArtistInfo() {
    if (allow_clicks) {
        if (document.getElementById("basic_info_artist_button").childElementCount === 0) {
            hideOtherButton();
            hideAllInfoSection();
            removeListenerAndButton("artist");

            createButtonRelated("artist_info_button", "0 0 0",
                "5 5 5", "basic_info_artist_button",
                displayArtistInfo, "/static/3d_models/artist_info_circle.gltf");

            createButtonRelated("artist_picture_button", "1 0 0",
                "5 5 5", "basic_info_artist_button",
                displayArtistPicture, "/static/3d_models/artist_picture_circle.gltf");

            createButtonRelated("artist_abstract", "2 0 0",
                "5 5 5", "basic_info_artist_button",
                displayArtistAbstract, "/static/3d_models/artist_description_circle.gltf");

            createButtonRelated("artist_influence", "3.05 0 0",
                "4.5 4.5 4.5", "basic_info_artist_button",
                displayArtistInfluence, "/static/3d_models/influence_circle.gltf");

            createButtonRelated("artist_influence_by", "4.2 0 0",
                "4.5 4.5 4.5", "basic_info_artist_button",
                displayArtistInfluenceBy, "/static/3d_models/influenced_by_circle.gltf");

            document.getElementById("button_related_to_functionality").setAttribute("visible", "true");
            document.getElementById('basic_info_artist_button').setAttribute("visible", "true");
        }
    }
}

function displayButtonAdvancedArtworkInfo() {
    if (allow_clicks) {
        if (document.getElementById("advanced_info_artwork_button").childElementCount === 0) {
            hideOtherButton();
            hideAllInfoSection();
            removeListenerAndButton("advanced");

            createButtonRelated("artwork_museum_button", "0 0 0",
                "4.5 4.5 4.5", "advanced_info_artwork_button",
                displayArtworkMuseumInfo, "/static/3d_models/artwork_museum_circle.gltf");

            createButtonRelated("artwork_same_artist_button", "1 0 0",
                "4.5 4.5 4.5", "advanced_info_artwork_button",
                displayArtworkSameArtistInfo, "/static/3d_models/artwork_same_artist_circle.gltf");

            createButtonRelated("artwork_same_century_button", "2 0 0",
                "4.5 4.5 4.5", "advanced_info_artwork_button",
                displayArtworkSameCenturyInfo, "/static/3d_models/artwork_same_century_circle.gltf");

            createButtonRelated("useful_links_button", "3 0 0",
                "4.5 4.5 4.5", "advanced_info_artwork_button",
                displayUsefulLinksInfo, "/static/3d_models/useful_links_circle.gltf");

            document.getElementById("button_related_to_functionality").setAttribute("visible", "true");
            document.getElementById('advanced_info_artwork_button').setAttribute("visible", "true");
        }
    }
}

function removeArtworkListener() {
    if (document.getElementById("basic_info_artwork_button").childElementCount !== 0) {
        document.querySelector("#artwork_artist_info")
            .removeEventListener('click', displayArtworkInfo);
        document.querySelector("#artwork_abstract")
            .removeEventListener('click', displayArtworkAbstract);
        document.getElementById("basic_info_artwork_button").innerHTML = "";
    }
}

function removeAdvancedArtworkListener() {
    if (document.getElementById("advanced_info_artwork_button").childElementCount !== 0) {
        document.querySelector("#artwork_museum_button")
            .removeEventListener('click', displayArtworkMuseumInfo);
        document.querySelector("#artwork_same_artist_button")
            .removeEventListener('click', displayArtworkSameArtistInfo);
        document.querySelector("#artwork_same_century_button")
            .removeEventListener('click', displayArtworkSameCenturyInfo);
        document.querySelector("#useful_links_button")
            .removeEventListener('click', displayUsefulLinksInfo);
        document.getElementById('advanced_info_artwork_button').innerHTML = "";
    }
}

function removeListenerArtist() {
    if (document.getElementById("basic_info_artist_button").childElementCount !== 0) {
        document.querySelector("#artist_info_button")
            .removeEventListener('click', displayArtistInfo);
        document.querySelector("#artist_picture_button")
            .removeEventListener('click', displayArtistPicture);
        document.querySelector("#artist_abstract")
            .removeEventListener('click', displayArtistAbstract);
        document.querySelector("#artist_influence_by")
            .removeEventListener('click', displayArtistInfluenceBy);
        document.querySelector("#artist_influence")
            .removeEventListener('click', displayArtistInfluence);
        document.getElementById("basic_info_artist_button").innerHTML = "";
    }
}

function removeListenerAndButton(type) {
    if (type === "artist") {
        removeArtworkListener();
        removeAdvancedArtworkListener();

    } else if (type === "advanced") {
        removeArtworkListener();
        removeListenerArtist();
    } else {
        removeListenerArtist();
        removeAdvancedArtworkListener();
    }
}

function hideOtherButton() {
    document.getElementById('basic_info_artwork_button').setAttribute("visible", "false");
    document.getElementById('basic_info_artist_button').setAttribute("visible", "false");
    document.getElementById('advanced_info_artwork_button').setAttribute("visible", "false");


}

function hideAllInfoSection() {
    document.getElementById("artwork_basic_info_area").setAttribute("visible", "false");
    document.getElementById("artwork_abstract_area").setAttribute("visible", "false");
    document.getElementById("artworkAbstract").setAttribute("visible", "false");
    document.getElementById("artist_basic_info_area").setAttribute("visible", "false");
    document.getElementById("artist_abstract_area").setAttribute("visible", "false");
    document.getElementById("artistPicture").setAttribute("visible", "false");
    document.getElementById("artistAbstract").setAttribute("visible", "false");
    document.getElementById("list").setAttribute("visible", "false");
    document.getElementById("real_links").setAttribute("visible", "false");
    document.getElementById("real_links_area").setAttribute("visible", "false");
    document.getElementById("no_links_area").setAttribute("visible", "false");
}

function displayData() {
    alert("The request is done. You can explore the Augmented Reality Interface.");
    hideOtherButton();
    hideAllInfoSection();
    if (!document.getElementById("anchor").getAttribute("url")) {
        console.log("reload page");
        location.reload();
    }
    document.getElementById("scene").setAttribute("visible", "true");
    document.getElementById("functionality").setAttribute("visible", "true");
    document.getElementById("camera").setAttribute("visible", "true");
    if (!artwork_presence) {
        displayArtworkPicture();
    }
    displayArtworkArtistYearText();
}

function askDataVisView() {
    var form = document.createElement('form');
    form.setAttribute("action", "/data_vis");
    form.setAttribute("method", "post");
    var hiddenField_1 = document.createElement('input');
    hiddenField_1.setAttribute('type', 'hidden');
    hiddenField_1.setAttribute('name', 'basic_info_artwork');
    hiddenField_1.setAttribute('value', JSON.stringify(basic_info_artwork));
    form.appendChild(hiddenField_1);
    var hiddenField_2 = document.createElement('input');
    hiddenField_2.setAttribute('type', 'hidden');
    hiddenField_2.setAttribute('name', 'basic_info_artist');
    hiddenField_2.setAttribute('value', JSON.stringify(basic_info_artist));
    form.appendChild(hiddenField_2);
    var hiddenField_3 = document.createElement('input');
    hiddenField_3.setAttribute('type', 'hidden');
    hiddenField_3.setAttribute('name', 'advanced_info_artwork');
    hiddenField_3.setAttribute('value', JSON.stringify(advanced_info_artwork));
    form.appendChild(hiddenField_3);
    document.body.appendChild(form);
    form.submit();
}