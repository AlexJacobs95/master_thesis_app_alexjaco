/*
* In this javascript file, it contains all the code needed to create on the fly
* the datable with the requests history.
* It contains function to function to redirect to other application functionalities
* and also to redirect the user to search interface or data visualition interface to see the request associated.
*/
if (artwork_resquest_history === null && custom_search_resquest_history === null) {
    createSection("Requests History ");
    table = document.createElement("table");
    table.setAttribute("id", "result_table");
    table.setAttribute("class", "display ");
    document.getElementById('table_container_Requests History').appendChild(table);
    var data = [];
    $('#result_table').DataTable({
        data: data,
        searching: true,
        lengthChange: true,
        columns: [
            {title: "Request Title", data: "title"},
            {title: "Request Type", data: "type"},
            {title: "Request Parameters", data: "parameters"}
        ]
    });
} else {
    displayResult();
}

function viewArtworkDataVis(data, i) {
    var form = document.createElement('form');
    form.setAttribute("action", "/data_vis");
    form.setAttribute("method", "post");
    var hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'artwork_name');
    hiddenField.setAttribute('value', data[i]["artwork"]);
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
}

function viewCustomSearch(data, i) {
    var form = document.createElement('form');
    form.setAttribute("action", "/custom_search");
    form.setAttribute("method", "post");
    var hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'keyword');
    hiddenField.setAttribute('value', data[i]["search"]);
    form.appendChild(hiddenField);
    hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'type');
    hiddenField.setAttribute('value', data[i]["parameters"][0]);
    form.appendChild(hiddenField);
    hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'artwork_list');
    hiddenField.setAttribute('value', data[i]["parameters"][1]);
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
}

function createSection(titre) {
    console.log("hey", "table_container_" + titre);
    var $element = $('#' + titre);
    if (!$element.length) {

        var section = document.createElement("SECTION");
        section.setAttribute('id', titre);
        section.setAttribute('class', "blue");
        section.setAttribute('style', "margin:110px; width:auto; height: auto");
        div_container = document.createElement('div');
        div_row = document.createElement('div');
        div_row.setAttribute('id', 'div_row');
        div_text = document.createElement('div');
        div_text.setAttribute("class", "text-center table_container");
        div_text.setAttribute("id", "table_container_" + titre);


        h2 = document.createElement('h2');
        h2.setAttribute("class", "titre-section");
        h2.innerHTML = titre;

        div_text.appendChild(h2);
        div_row.appendChild(div_text);
        div_container.appendChild(div_row);
        section.appendChild(div_container);
        document.body.appendChild(section);
    }
}


function displayResult() {
    var typeMapping = {
        "material": "Material",
        "type_technique": "Type/Technique",
        "art_mov": "Art Movement",
        "museum": "Museum",
        "artist": "Artist",
        "artwork": "Artwork"
    }
    createSection("History Requests");
    table = document.createElement("table");
    table.setAttribute("id", "result_table");
    table.setAttribute("class", "display ");
    document.getElementById('table_container_History Requests').appendChild(table);
    let i;
    var data = [];
    if (artwork_resquest_history !== null) {
        for (i = 0; i < artwork_resquest_history.length; i++) {
            var dataToPush;
            var href_link = 'javascript:viewArtworkDataVis(artwork_resquest_history,' + i.toString() + ')';
            var ref = '<a href="' + href_link + '" style="color:blue";>' + artwork_resquest_history[i]["artwork"] + '</a>'
            dataToPush = {"title": ref, "type": "Artwork", "parameters": "NA"};
            data.push(dataToPush);
        }
    }
    if (custom_search_resquest_history !== null) {
        for (i = 0; i < custom_search_resquest_history.length; i++) {
            var dataToPush;
            var href_link = 'javascript:viewCustomSearch(custom_search_resquest_history,' + i.toString() + ')';
            var ref = '<a href="' + href_link + '" style="color:blue";>' + custom_search_resquest_history[i]["search"] + '</a>'
            dataToPush = {
                "title": ref,
                "type": "Custom Search",
                "parameters": "Type of keywords: " + typeMapping[custom_search_resquest_history[i]["parameters"][0]] + "; List of Artworks: " + custom_search_resquest_history[i]["parameters"][1]
            };
            data.push(dataToPush);
        }
    }
    $('#result_table').DataTable({
        data: data,
        searching: true,
        lengthChange: true,
        columns: [
            {title: "Request Title", data: "title"},
            {title: "Request Type", data: "type"},
            {title: "Request Parameters", data: "parameters"}
        ]
    });
}
