/*
* This code is based on the code of Juan Mellado
* https://github.com/jcmellado/js-aruco
*
*/


var video, canvas, context, imageData, detector;
var modelSize = 35.0; //millimeters
var basic_info_artwork;
var basic_info_artist;
var advanced_info_artwork;

console.log(lastArucoIdScan);

function onLoad() {
    video = document.getElementById("video");
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");

    canvas.width = parseInt(canvas.style.width);
    canvas.height = parseInt(canvas.style.height);

    if (navigator.mediaDevices === undefined) {
        navigator.mediaDevices = {};
    }

    if (navigator.mediaDevices.getUserMedia === undefined) {
        navigator.mediaDevices.getUserMedia = function (constraints) {
            var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            if (!getUserMedia) {
                return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
            }

            return new Promise(function (resolve, reject) {
                getUserMedia.call(navigator, constraints, resolve, reject);
            });
        }
    }

    navigator.mediaDevices
        .getUserMedia({video: {facingMode: 'environment'}})
        .then(function (stream) {
            if ("srcObject" in video) {
                video.srcObject = stream;
            } else {
                video.src = window.URL.createObjectURL(stream);
            }
        })
        .catch(function (err) {
                console.log(err.name + ": " + err.message);
            }
        );
    detector = new AR.Detector();
    requestAnimationFrame(tick);
}

function tick() {
    requestAnimationFrame(tick);
    if (video.readyState === video.HAVE_ENOUGH_DATA) {
        snapshot();
        var markers = detector.detect(imageData);
        var corners, corner, i;

        if (markers.length > 0) {
            corners = markers[0].corners;

            for (i = 0; i < corners.length; ++i) {
                corner = corners[i];

                corner.x = corner.x - (canvas.width / 2);
                corner.y = (canvas.height / 2) - corner.y;
            }
            if (lastArucoIdScan !== markers[0].id) {
                lastArucoIdScan = markers[0].id;
                console.log("request send");
                console.log(markers[0].id);
                console.log(scan_marker);
                if (scan_marker) {
                    sendIdAndPosition("/artwork_info",
                        {
                            "marker": markers[0].id
                        });
                }

            } else {
                console.log("already scan, request avoid")
            }
        }
    }
}

function snapshot() {
    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    imageData = context.getImageData(0, 0, canvas.width, canvas.height);
}

function sendIdAndPosition(url, dataToSend, method = 'post') {
    var form = document.createElement('form');
    form.setAttribute("action", url);
    form.setAttribute("method", method);
    var hiddenField = document.createElement('input');
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute('name', 'marker');
    hiddenField.setAttribute('value', dataToSend["marker"]);
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
}

window.onload = onLoad;